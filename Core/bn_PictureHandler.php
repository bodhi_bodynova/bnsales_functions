<?php

namespace Bodynova\bnSales_Functions\Core;

class bn_PictureHandler extends bn_PictureHandler_parent
{
    public function getPicUrl($sPath, $sFile, $sSize, $sIndex = null, $sAltPath = false, $bSsl = null)
    {
        $sDirName = '900_900_75';// . $this->getConfig()->getConfigParam('sDefaultImageQuality');
        return 'https://bodynova.de/out/pictures/generated/product/1/' . $sDirName . '/' . $sFile;
    }
}