<?php

namespace Bodynova\bnSales_Functions\Core;

class bn_Session extends bn_Session_parent{
    /**
     * Save Basket to Session
     * @param oxBasket $oBasket
     * @return $this
     */
    public function saveBasket()
    {
        return $this->setVariable($this->_getBasketName(), serialize($this->getBasket()));
        /*
        $this->setBasket($oBasket);
        $this->setVariable($this->_getBasketName(), serialize($oBasket));
        return $this;
        */
    }
}



