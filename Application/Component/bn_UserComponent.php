<?php

namespace Bodynova\bnSales_Functions\Application\Component;

class bn_UserComponent extends bn_UserComponent_parent{

    protected $arUserFavorites = 'test';


    /**
     * Executes parent::render(), oxcmp_user::_loadSessionUser(), loads user delivery
     * info. Returns user object oxcmp_user::oUser.
     *
     * @return  object  user object
     */
    public function render()
    {
        parent::render();

        return $this->getUser();
    }


    /**
     * Add flag "forcechangepassword=1" to url an redirects to password change form to a force a password
     * change when the user passwor still is "bodynova"
     *
     * @param $oUser
     */
    protected function _afterLogin($oUser)
    {
        // call parent
        $res = parent::_afterLogin($oUser);
        //
        if ($_POST['lgn_pwd'] === 'bodynova') {
            \OxidEsales\Eshop\Core\Registry::getUtils()->redirect($this->getConfig()->getShopHomeURL() . 'cl=account_password&forcechangepassword=1');
        }
        return 'start';
        // ende
    }


    /**
     * to avoid the basked moved to a non user session (per default the haendlershop forces a logged in user
     * we do not move the user basket back into a session where no user is present
     */
    protected function _afterLogout()
    {
        parent::_afterLogout();
        $myConfig = $this->getConfig();
        \OxidEsales\Eshop\Core\Registry::getUtils()->redirect($myConfig->getShopCurrentUrl() . 'cl=account&sourcecl=start');
        // ende
    }


    /**
     * Deletes user information from session:<br>
     * "usr", "dynvalue", "paymentid"<br>
     * also deletes cookie, unsets \OxidEsales\Eshop\Core\Config::oUser,
     * oxcmp_user::oUser, forces basket to recalculate.
     *
     * @return null
     */
    public function logout()
    {
        parent::logout();
        $myConfig = $this->getConfig();
        //weiterleitung funktioniert bei nicht angemeldet bleiben:
        \OxidEsales\Eshop\Core\Registry::getUtils()->redirect($myConfig->getShopCurrentUrl() . 'cl=account&sourcecl=start');
    }

    public function getUserFavorites()
    {
        return $this->arUserFavorites;
    }
}