<?php

namespace Bodynova\bnSales_Functions\Application\Controller;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;

class bn_myfavorites extends \OxidEsales\Eshop\Application\Controller\FrontendController
{

    // teplate for the view
    protected $_sThisTemplate = null;
    public $order = null;
    public $orderdate = null;

    public function render()
    {

        parent::render();

        $oTheme = oxNew(\OxidEsales\Eshop\Core\Theme::class);
        $theme = $this->_sActiveTheme = $oTheme->getActiveThemeId();

        $oUser = $this->getUser();
        $regobject = Registry::getConfig();

        if ($oUser == null) {
            Registry::getUtils()->redirect($regobject->getShopHomeURL() . 'cl=account&sourcecl=start');
        }
        if($theme == 'bnsales_wave_child'){
            if($_GET['orderid'] != '' || $_GET['fnc'] == 'showOrderedArticles') {
                $this->order = 'order';
            }
            return 'bn_bnchild_myfavorites.tpl';
        } else {
            return 'bn_myfavorites.tpl';
        }

    }


    /**
     * Returns Bread Crumb - you are here page1/page2/page3...
     *
     * @return array
     */
    public function getBreadCrumb()
    {
        $aPaths = array();
        $aPath = array();
        $oLang = Registry::getLang();
        $iBaseLanguage = $oLang->getBaseLanguage();

        $aPath['title'] = $oLang->translateString('BTN_MY_FAVORITES', $iBaseLanguage, false);

        $aPath['link'] = $this->getLink();

        $aPaths[] = $aPath;

        return $aPaths;
    }

    /**
     *
     */
    public function showLists()
    {
        $oUser = $this->getUser();
        $regobject = Registry::getConfig();

        if ($oUser == null) {
            Registry::getUtils()->redirect($regobject->getShopHomeURL() . 'cl=account&sourcecl=start');
        }
        //
        $oFavoritesList = oxNew("oxlist");
        $oFavoritesList->init("oxbase");
        $strQuery = 'SELECT OXARTICLE FROM oxfavorites WHERE OXUSER=\'' . $this->getUser()->getId() . '\'';
        $oFavoritesList->selectString($strQuery);

        // speichere die favorisierten artikel in ein array
        $arrFavoritesArticles = array();
        if ($oFavoritesList->count()) {
            foreach ($oFavoritesList as $oItem) {
                $arrFavoritesArticles[] = $oItem->__oxarticle->rawValue;
            }
        }

        $oArtList = oxNew('oxarticlelist');
        $oArtList->loadByArticlesIDs($arrFavoritesArticles);

        //
        $this->_aViewData['type'] = 'line';
        $this->_aViewData['products'] = $oArtList;
        // ende
    }


    /**
     *
     */
    public function toggleFavoritEntry()
    {
        //
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        //
        $sSelect = 'SELECT * FROM oxfavorites WHERE OXARTICLE= \'' . $_GET['oxid'] . '\' AND ' .
            ' OXUSER= \'' . $this->getUser()->getId() . '\'';
        //und
        $aData = $oDb->getRow($sSelect);
        //
        $res = false;
        //
        if (count($aData)) {
            //
            $sStatement = 'DELETE FROM oxfavorites WHERE '
                . ' OXARTICLE= \'' . $_GET['oxid'] . '\' AND '
                . ' OXUSER= \'' . $this->getUser()->getId() . '\' ';
            //
            $oDb->Execute($sStatement);
            // ende
        } else {
            //
            $sStatement = 'INSERT into oxfavorites SET '
                . ' OXARTICLE= \'' . $_GET['oxid'] . '\', '
                . ' OXUSER= \'' . $this->getUser()->getId() . '\' ';
            //
            $oDb->Execute($sStatement);
            //
            $res = true;
            // ende
        }
        //
        die(json_encode(array('added' => $res, 'oxid' => $_GET['oxid'])));
        // ende
    }


    /**
     *
     */
    function showOrderedArticles()
    {
        //
        $oUser = $this->getUser();
        $regobject = Registry::getConfig();

        if ($oUser == null) {
            Registry::getUtils()->redirect($regobject->getShopHomeURL() . 'cl=account&sourcecl=start');
        }

        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        //
        $sQ = 'select oxid from oxorder where oxuserid = ' . $oDb->quote($this->getUser()->getId()) . ' and oxid = ' . $oDb->quote($_GET['orderid']);
        //
        $res = DatabaseProvider::getDb()->getOne($sQ);
        //
        if ($res != $_GET['orderid']) {
            Registry::getUtils()->redirect($this->getConfig()->getShopHomeURL() . 'cl=account_order', false, 301);
            exit;
        }

        //
        $oOrder = oxNew("oxorder");
        $oOrder->load($_GET['orderid']);
        $oOrderArticleList = $oOrder->getOrderArticles();

        //
        $arrArticleIDs = $arrArticleIDAmounts = array();
        foreach ($oOrderArticleList AS $oOrderArticle) {
            $arrArticleIDs[] = $oOrderArticle->oxorderarticles__oxartid->getRawValue();
            $arrArticleIDAmounts[$oOrderArticle->oxorderarticles__oxartid->getRawValue()] = $oOrderArticle->oxorderarticles__oxamount->rawValue;
        }

        //
        $oArtList = oxNew('oxarticlelist');
        $oArtList->loadByArticlesIDs($arrArticleIDs);

        //
        $this->_aViewData['type'] = 'line';

        // merge amounts into articles set
        foreach ($oArtList AS $key => $oArt) {
            $oArt->orderedamount = $arrArticleIDAmounts[$oArt->getId()];
            $oArtList[$key] = $oArt;
        }

        //
        $this->_aViewData['isorder'] = true;
        $this->_aViewData['products'] = $oArtList;
        $this->orderdate = date('d.m.Y',strtotime($oOrder->oxorder__oxorderdate->value));
        // ende
    }
}
