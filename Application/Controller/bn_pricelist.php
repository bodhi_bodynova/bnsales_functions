<?php

namespace Bodynova\bnSales_Functions\Application\Controller;

use bn_pricelistpdf;
use Bodynova\bnSales_Functions\Application\Model\bn_ArticleList;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Helper\Html;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


require_once dirname(__FILE__) . '/bn_pricelistpdf.php';

class bn_pricelist extends \OxidEsales\Eshop\Application\Controller\FrontendController
{
    protected $strImageSize = '150_150_100';

    /**
     *
     */
    public function getPDF()
    {

        $oUser = $this->getUser();
        if ($oUser) {
            if ($oUser->inGroup('oxidpricea')) {
                $sPriceSuffix = 'a';
                $sAktionsBeginn = 'oxarticles__oxpricea_aktionsbeginn';
                $sAktionsEnde = 'oxarticles__oxpricea_aktionsende';
                $sAktionsPreis = 'oxarticles__oxpricea_aktionspreis';
            } elseif ($oUser->inGroup('oxidpriceb')) {
                $sPriceSuffix = 'b';
                $sAktionsBeginn = 'oxarticles__oxpriceb_aktionsbeginn';
                $sAktionsEnde = 'oxarticles__oxpriceb_aktionsende';
                $sAktionsPreis = 'oxarticles__oxpriceb_aktionspreis';
            } elseif ($oUser->inGroup('oxidpricec')) {
                $sPriceSuffix = 'c';
                $sAktionsBeginn = 'oxarticles__oxpricec_aktionsbeginn';
                $sAktionsEnde = 'oxarticles__oxpricec_aktionsende';
                $sAktionsPreis = 'oxarticles__oxpricec_aktionspreis';
            } elseif ($oUser->inGroup('oxidpriced')) {
                $sPriceSuffix = 'd';
                $sAktionsBeginn = 'oxarticles__oxpriced_aktionsbeginn';
                $sAktionsEnde = 'oxarticles__oxpriced_aktionsende';
                $sAktionsPreis = 'oxarticles__oxpriced_aktionspreis';
            } elseif ($oUser->inGroup('oxidpricee')) {
                $sPriceSuffix = 'e';
                $sAktionsBeginn = 'oxarticles__oxpricee_aktionsbeginn';
                $sAktionsEnde = 'oxarticles__oxpricee_aktionsende';
                $sAktionsPreis = 'oxarticles__oxpricee_aktionspreis';
            }
        }
        if (!$oUser) {
            die('Sie sind nicht angemeldet.');
        }
        $lang = Registry::getLang()->getObjectTplLanguage();

        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);
        // bekomme alle artikel

        $objArticleListe = new bn_ArticleList();

        if ($_GET['name'] == 'order') {
            $query = 'SELECT OXARTID FROM oxorderarticles LEFT JOIN oxarticles ON oxorderarticles.OXARTID = oxarticles.OXID WHERE OXORDERID IN (SELECT OXID FROM oxorder WHERE OXUSERID = ?)';
            $arrUser = array($oUser->getId());
            $arrOxid = array();
            $result = $oDb->getAll($query, $arrUser);
            foreach ($result as $key) {
                array_push($arrOxid, $key[0]);
            }
            $objArticleListe->loadByArticlesIDs($arrOxid);
        } else {
            if (isset($_GET['cat'])) {
                $objArticleListe->loadCategoryArticles($_GET['cat'], null);
            } else {
                /*if ($oUser) {
                    if ($oUser->inGroup('oxidpricea')) {
                        $sPriceSuffix = 'a';
                    } elseif ($oUser->inGroup('oxidpriceb')) {
                        $sPriceSuffix = 'b';
                    } elseif ($oUser->inGroup('oxidpricec')) {
                        $sPriceSuffix = 'c';
                    } elseif ($oUser->inGroup('oxidpriced')) {
                        $sPriceSuffix = 'd';
                    } elseif ($oUser->inGroup('oxidpricee')) {
                        $sPriceSuffix = 'e';
                    }
                }*/
                if(file_exists(dirname(__DIR__,5) . '/export/pdf/' . md5('preisliste' . $sPriceSuffix . $lang) . '.pdf' )){

                    $file = dirname(__DIR__,5) . '/export/pdf/' . md5('preisliste' . $sPriceSuffix . $lang) . '.pdf';

                    header("Content-type:application/pdf");

                    header("Content-Disposition:attachment;filename='". md5('preisliste' . $sPriceSuffix . $lang) ."'");

                    //header("Content-Disposition: attachment; filename=\"" . str_replace('/', '',$_GET['name']).$_GET['user'] . '.xlsx' . "\"");

                    readfile($file);
                    die();
                }
                $objArticleListe->loadPriceArticles(0, 999999999);
            }
        }

        // array zum speichern der artikel
        $arrElemente = array();

        // laufe über alel artikel und speichere sie in dem array
        foreach ($objArticleListe AS $objArticle) {

            $ampel = null;
            switch ($objArticle->oxarticles__bnflagbestand->value) {
                case 0 :
                    $ampel = Registry::getLang()->translateString('EXCEL_AMPEL_0');
                    break;
                case 1 :
                    $ampel = Registry::getLang()->translateString('EXCEL_AMPEL_1');
                    break;
                case 2 :
                    $ampel = Registry::getLang()->translateString('EXCEL_AMPEL_2');
                    break;
            }

            // bei varianten: lade diese und speichere sie in dem array andernfalls den einfachen artikel
            if ($objArticle->getVariantsCount()) {
                foreach ($objArticle->getFullVariants() AS $objVariante) {
                    $ampel = null;
                    switch ($objVariante->oxarticles__bnflagbestand->value) {
                        case 0 :
                            $ampel = Registry::getLang()->translateString('EXCEL_AMPEL_0');
                            break;
                        case 1 :
                            $ampel = Registry::getLang()->translateString('EXCEL_AMPEL_1');
                            break;
                        case 2 :
                            $ampel = Registry::getLang()->translateString('EXCEL_AMPEL_2');
                            break;
                    }
                    $arrElemente[] = array(
                        'name' => str_replace("&amp;", "&", str_replace("&quot;", "\"", $objArticle->oxarticles__oxtitle->value)),
                        'varname' => str_replace("&amp;", "&", str_replace("&quot;", "\"", $objVariante->oxarticles__oxvarselect->value)),
                        //'bild'           =>  null, //$objVariante->getPictureUrl(),
                        //'bild'              =>  ($objVariante->oxarticles__oxpic1->value ? $objVariante->oxarticles__oxpic1->value : 'https://bodynova.de/out/imagehandler.php?artnum='.$objVariante->oxarticles__oxartnum->value.'&size='.$this->strImageSize),
                        'artikelnummer' => $objVariante->oxarticles__oxartnum->value,
                        'aktionspreis' =>$objVariante->$sAktionsPreis->value == 0 ? "" : $objVariante->getPrice()->getPrice(), //($objVariante->$sAktionsPreis->value > 0 ? number_format($objVariante->$sAktionsPreis->value,2) : ""),
                        'preis' => $objVariante->$sAktionsPreis->value == 0 ?  $objVariante->getPrice()->getPrice():number_format($objVariante->$sAktionsPreis->value,2),//$objVariante->getPrice()->getPrice(),
                        'uvp' => $objVariante->oxarticles__oxprice->value,
                        'ean' => ($objVariante->oxarticles__oxean->value ? $objVariante->oxarticles__oxean->value : null),
                        'ampel' => $ampel,
                        'gewicht' => $objVariante->oxarticles__oxweight->value,
                        //'aktion' => ($objVariante->oxarticles__bnflagaktion->value == 0 ? null : $objVariante->oxarticles__bnflagaktion->value),
                        'auslaufartikel' => ($objVariante->oxarticles__bnflagauslaufartikel->value == 0 ? null : $objVariante->oxarticles__bnflagauslaufartikel->value),
                        'aktionende' => ($objVariante->$sAktionsEnde->value != '0000-00-00' && $objVariante->$sAktionsEnde->value > date('Y-m-d') && $objVariante->$sAktionsBeginn->value < date('Y-m-d') ? date('d.m.Y', strtotime($objVariante->$sAktionsEnde->value)) : ''),
                        //'test' => $objVariante->getArticleUser()
                    );
                }
            } else {
                $arrElemente[] = array(
                    'name' => str_replace("&amp;", "&", str_replace("&quot;", "\"", $objArticle->oxarticles__oxtitle->value)),
                    'varname' => str_replace("&amp;", "&", str_replace("&quot;", "\"", $objArticle->oxarticles__oxvarselect->value)), //null
                    //'bild'              =>  ($objArticle->oxarticles__oxpic1->value ? $objArticle->oxarticles__oxpic1->value : 'https://bodynova.de/out/imagehandler.php?artnum='.$objArticle->oxarticles__oxartnum->value.'&size='.$this->strImageSize),
                    'artikelnummer' => $objArticle->oxarticles__oxartnum->value,
                    'aktionspreis' =>$objArticle->$sAktionsPreis->value == 0 ? "" : $objArticle->getPrice()->getPrice(), //($objArticle->$sAktionsPreis->value > 0 ? number_format($objArticle->$sAktionsPreis->value,2) : ""),
                    'preis' => $objArticle->$sAktionsPreis->value == 0 ?  $objArticle->getPrice()->getPrice():number_format($objArticle->$sAktionsPreis->value,2),//$objArticle->getPrice()->getPrice(),
                    'uvp' => $objArticle->oxarticles__oxprice->value,
                    'ean' => ($objArticle->oxarticles__oxean->value ? $objArticle->oxarticles__oxean->value : null),
                    'ampel' => $ampel,
                    'gewicht' => $objArticle->oxarticles__oxweight->value,
                    //'aktion' => ($objArticle->oxarticles__bnflagaktion->value == 0 ? null : $objArticle->oxarticles__bnflagaktion->value),
                    'auslaufartikel' => ($objArticle->oxarticles__bnflagauslaufartikel->value == 0 ? null : $objArticle->oxarticles__bnflagauslaufartikel->value),
                    'aktionende' => ($objArticle->$sAktionsEnde->value != '0000-00-00' && $objArticle->$sAktionsEnde->value > date('Y-m-d') && $objArticle->$sAktionsBeginn->value < date('Y-m-d') ? date('d.m.Y', strtotime($objArticle->$sAktionsEnde->value)) : ''),
                );
            }
        }

        usort($arrElemente, function ($a, $b) {
            return $a['name'] > $b['name'];
        });


        // initiating pdf engine
        $oPdf = new bn_pricelistpdf();

        //
        foreach ($arrElemente AS $arrElement) {
            $oPdf->writeBnCell(
                $arrElement['artikelnummer'],
                $arrElement['name'],
                $arrElement['varname'],
                $arrElement['aktionspreis'],//number_format($arrElement['aktionspreis'],2),
                number_format($arrElement['preis'], 2),
                number_format($arrElement['uvp'], 2),
                $arrElement['ean'],
                $arrElement['ampel'],
                $arrElement['gewicht'],
                $arrElement['aktion'],
                $arrElement['auslaufartikel'],
                $arrElement['aktionende']
            );
        }
        $oPdf->lastPage();

        /*
        echo '<pre>';

        //echo php_sapi_name();
        echo ob_get_contents();
        echo headers_sent();
        //print_r($oPdf);
        die();
        */

        //
        //die(dirname(__DIR__,5));
        $oPdf->Output(dirname(__DIR__, 5) . '/export/pdf/preisliste.pdf', 'FI');
    }

    /**
     * Funktion für Excel-Liste, abhängig ob Alle Artikel oder Kategorieabhängig oder Bestellhistorie...
     */
    public function getExcel()
    {


        $lang = Registry::getLang()->getObjectTplLanguage();

        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);
        $oUser = $this->getUser();
        if ($oUser) {
            if ($oUser->inGroup('oxidpricea')) {
                $sPriceSuffix = 'a';
                $sAktionsBeginn = 'oxarticles__oxpricea_aktionsbeginn';
                $sAktionsEnde = 'oxarticles__oxpricea_aktionsende';
                $sAktionsPreis = 'oxarticles__oxpricea_aktionspreis';
                $spreisneu = 'oxarticles__preisneua';
            } elseif ($oUser->inGroup('oxidpriceb')) {
                $sPriceSuffix = 'b';
                $sAktionsBeginn = 'oxarticles__oxpriceb_aktionsbeginn';
                $sAktionsEnde = 'oxarticles__oxpriceb_aktionsende';
                $sAktionsPreis = 'oxarticles__oxpriceb_aktionspreis';
                $spreisneu = 'oxarticles__preisneub';
            } elseif ($oUser->inGroup('oxidpricec')) {
                $sPriceSuffix = 'c';
                $sAktionsBeginn = 'oxarticles__oxpricec_aktionsbeginn';
                $sAktionsEnde = 'oxarticles__oxpricec_aktionsende';
                $sAktionsPreis = 'oxarticles__oxpricec_aktionspreis';
                $spreisneu = 'oxarticles__preisneuc';
            } elseif ($oUser->inGroup('oxidpriced')) {
                $sPriceSuffix = 'd';
                $sAktionsBeginn = 'oxarticles__oxpriced_aktionsbeginn';
                $sAktionsEnde = 'oxarticles__oxpriced_aktionsende';
                $sAktionsPreis = 'oxarticles__oxpriced_aktionspreis';
                $spreisneu = 'oxarticles__preisneud';
            } elseif ($oUser->inGroup('oxidpricee')) {
                $sPriceSuffix = 'e';
                $sAktionsBeginn = 'oxarticles__oxpricee_aktionsbeginn';
                $sAktionsEnde = 'oxarticles__oxpricee_aktionsende';
                $sAktionsPreis = 'oxarticles__oxpricee_aktionspreis';
                $spreisneu = 'oxarticles__preisneue';
            }
        }
        // bekomme alle artikel
        $objArticleListe = new bn_ArticleList();
        if ($_GET['name'] == 'order') {
            $query = 'SELECT DISTINCT OXARTID FROM oxorderarticles LEFT JOIN oxarticles ON oxorderarticles.OXARTID = oxarticles.OXID WHERE OXORDERID IN (SELECT OXID FROM oxorder WHERE OXUSERID = ?)';
            $arrUser = array($oUser->getId());
            $arrOxid = array();
            $result = $oDb->getAll($query, $arrUser);

            foreach ($result as $key) {
                array_push($arrOxid, $key[0]);
            }
            $objArticleListe->loadByArticlesIDs($arrOxid);

        } else {

            if (isset($_GET['cat'])) {
                $objArticleListe->loadCategoryArticles($_GET['cat'], null);
            } else {

                if(file_exists(dirname(__DIR__,5) . '/export/excel/' . md5('preislistExcel' . $sPriceSuffix . $lang) . '.xlsx')){
                    $file = dirname(__DIR__) . '../../../../../export/excel/' . md5('preislistExcel' . $sPriceSuffix . $lang) . '.xlsx';

                    header("Content-Description: File Transfer");
                    header("Content-Type: application/octet-stream");
                    header('Content-Transfer-Encoding: binary');
                    #header('Content-Disposition: attachment; filename="' . $_GET['name'] . $_GET['user'] . '.xlsx"');
                    header('Content-Disposition: attachment; filename="' . md5('preislistExcel' . $sPriceSuffix . $lang) . '.xlsx"');
                    ob_clean();
                    readfile($file);
                    exit();
                }
                $objArticleListe->loadPriceArticles(0, 999999999);
            }
        }
        /*
                echo '<pre>';
                print_r($objArticleListe);
                die();
        */

        // array zum speichern der artikel
        $arrElemente = array();

        // laufe über alel artikel und speichere sie in dem array
        foreach ($objArticleListe AS $objArticle) {
            $ampel = null;
            #$objArticle->__set($objArticle->_blUseLazyLoading, false);
            switch ($objArticle->oxarticles__bnflagbestand->value) {
                case 0 :
                    $ampel = Registry::getLang()->translateString('EXCEL_AMPEL_0');
                    break;
                case 1 :
                    $ampel = Registry::getLang()->translateString('EXCEL_AMPEL_1');
                    break;
                case 2 :
                    $ampel = Registry::getLang()->translateString('EXCEL_AMPEL_2');
                    break;
            }

            // bei varianten: lade diese und speichere sie in dem array andernfalls den einfachen artikel
            if ($objArticle->getVariantsCount()) {
                foreach ($objArticle->getFullVariants(true, true) AS $objVariante) {
                    $ampel = null;
                    switch ($objVariante->oxarticles__bnflagbestand->value) {
                        case 0 :
                            $ampel = Registry::getLang()->translateString('EXCEL_AMPEL_0');
                            break;
                        case 1 :
                            $ampel = Registry::getLang()->translateString('EXCEL_AMPEL_1');
                            break;
                        case 2 :
                            $ampel = Registry::getLang()->translateString('EXCEL_AMPEL_2');
                            break;
                    }
                    $arrElemente[] = array(
                        'name' => str_replace("&amp;", "&", str_replace("&quot;", "\"", $objArticle->oxarticles__oxtitle->value)),
                        'varname' => str_replace("&amp;", "&", str_replace("&quot;", "\"", $objVariante->oxarticles__oxvarselect->value)),
                        //'bild'           =>  null, //$objVariante->getPictureUrl(),
                        //'bild'              =>  ($objVariante->oxarticles__oxpic1->value ? $objVariante->oxarticles__oxpic1->value : 'https://bodynova.de/out/imagehandler.php?artnum='.$objVariante->oxarticles__oxartnum->value.'&size='.$this->strImageSize),
                        'artikelnummer' => $objVariante->oxarticles__oxartnum->value,
                        'aktionspreis' =>$objVariante->$sAktionsPreis->value == 0 ? "" : $objVariante->getPrice()->getPrice(), //($objVariante->$sAktionsPreis->value > 0 ? number_format($objVariante->$sAktionsPreis->value,2) : ""),
                        'preis' => $objVariante->$sAktionsPreis->value == 0 ?  $objVariante->getPrice()->getPrice():number_format($objVariante->$sAktionsPreis->value,2),//$objVariante->getPrice()->getPrice(),
                        'preisneu' =>$objVariante->$spreisneu->value == 0 ? "" : $objVariante->$spreisneu->value,
                        'preisneuUvp' =>$objVariante->oxarticles__oxupdatepriceuvp->value == 0 ? "" : $objVariante->oxarticles__oxupdatepriceuvp->value,
                        'uvp' => $objVariante->oxarticles__oxprice->value,
                        'ean' => ($objVariante->oxarticles__oxean->value ? $objVariante->oxarticles__oxean->value : null),
                        'ampel' => $ampel,
                        'gewicht' => $objVariante->oxarticles__oxweight->value,
                        //'aktion' => ($objVariante->oxarticles__bnflagaktion->value == 0 ? null : $objVariante->oxarticles__bnflagaktion->value),
                        'auslaufartikel' => ($objVariante->oxarticles__bnflagauslaufartikel->value == 0 ? null : $objVariante->oxarticles__bnflagauslaufartikel->value),
                        'aktionende' => ($objVariante->$sAktionsEnde->value != '0000-00-00' && $objVariante->$sAktionsEnde->value > date('Y-m-d') && $objVariante->$sAktionsBeginn->value < date('Y-m-d') ? date('d.m.Y', strtotime($objVariante->$sAktionsEnde->value)) : ''),
                        //'test' => $objVariante->getArticleUser()
                    );
                }
            } else {
                $arrElemente[] = array(
                    'name' => str_replace("&amp;", "&", str_replace("&quot;", "\"", $objArticle->oxarticles__oxtitle->value)),
                    'varname' => str_replace("&amp;", "&", str_replace("&quot;", "\"", $objArticle->oxarticles__oxvarselect->value)), //null
                    //'bild'              =>  ($objArticle->oxarticles__oxpic1->value ? $objArticle->oxarticles__oxpic1->value : 'https://bodynova.de/out/imagehandler.php?artnum='.$objArticle->oxarticles__oxartnum->value.'&size='.$this->strImageSize),
                    'artikelnummer' => $objArticle->oxarticles__oxartnum->value,
                    'aktionspreis' =>$objArticle->$sAktionsPreis->value == 0 ? "" : $objArticle->getPrice()->getPrice(), //($objArticle->$sAktionsPreis->value > 0 ? number_format($objArticle->$sAktionsPreis->value,2) : ""),
                    'preis' => $objArticle->$sAktionsPreis->value == 0 ?  $objArticle->getPrice()->getPrice():number_format($objArticle->$sAktionsPreis->value,2),//$objArticle->getPrice()->getPrice(),
                    'preisneu' =>$objArticle->$spreisneu->value == 0 ? "" : $objArticle->$spreisneu->value,
                    'preisneuUvp' =>$objArticle->oxarticles__oxupdatepriceuvp->value == 0 ? "" : $objArticle->oxarticles__oxupdatepriceuvp->value,
                    'uvp' => $objArticle->oxarticles__oxprice->value,
                    'ean' => ($objArticle->oxarticles__oxean->value ? $objArticle->oxarticles__oxean->value : null),
                    'ampel' => $ampel,
                    'gewicht' => $objArticle->oxarticles__oxweight->value,
                    //'aktion' => ($objArticle->oxarticles__bnflagaktion->value == 0 ? null : $objArticle->oxarticles__bnflagaktion->value),
                    'auslaufartikel' => ($objArticle->oxarticles__bnflagauslaufartikel->value == 0 ? null : $objArticle->oxarticles__bnflagauslaufartikel->value),
                    'aktionende' => ($objArticle->$sAktionsEnde->value != '0000-00-00' && $objArticle->$sAktionsEnde->value > date('Y-m-d') && $objArticle->$sAktionsBeginn->value < date('Y-m-d') ? date('d.m.Y', strtotime($objArticle->$sAktionsEnde->value)) : ''),
                );
            }
            // TODO: Falls Aktion noch nicht begonnen, Enddatum ausblenden
            /*
                        echo '<pre>';
                        print_r($objVariante);
                        echo '</pre>';
            */
        }

        #print_r($arrElemente);
        #die();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 2;
        // TODO: Gewicht, Ampel(Lieferbar,...), Aktionauslaufdatum, Aktion beendet,
        $sheet->setCellValue('A1', Registry::getLang()->translateString('EXCEL_ARTNUM'))
            ->setCellValue('B1', Registry::getLang()->translateString('EXCEL_NAME'))
            ->setCellValue('C1', Registry::getLang()->translateString('EXCEL_VARNAME'))
            ->setCellValue('D1', Registry::getLang()->translateString('EXCEL_PRICEACTION'))
            ->setCellValue('E1', Registry::getLang()->translateString('EXCEL_PRICE'))
            ->setCellValue('F1', Registry::getLang()->translateString('EXCEL_UVP'))
            ->setCellValue('G1', Registry::getLang()->translateString('EXCEL_EAN'))
            //->setCellValue('G1',Registry::getLang()->translateString('EXCEL_AMPEL'))
            ->setCellValue('H1', Registry::getLang()->translateString('EXCEL_GEWICHT'))
            //->setCellValue('I1', Registry::getLang()->translateString('EXCEL_AKTION'))
            ->setCellValue('I1', Registry::getLang()->translateString('EXCEL_AKTIONENDE'))
            ->setCellValue('J1', Registry::getLang()->translateString('EXCEL_AUSLAUFARTIKEL'))
            ->setCellValue('K1', Registry::getLang()->translateString('EXCEL_PRICENEW'))
            ->setCellValue('L1', Registry::getLang()->translateString('EXCEL_PRICENEW') . " " . Registry::getLang()->translateString('EXCEL_UVP'));
        foreach ($arrElemente as $element) {
            $sheet->setCellValue('A' . $i, $element['artikelnummer'])
                ->setCellValue('B' . $i, $element['name'])
                ->setCellValue('C' . $i, $element['varname'])
                ->setCellValue('D' . $i, $element['aktionspreis'])
                ->setCellValue('E' . $i, $element['preis'])
                ->setCellValue('F' . $i, $element['uvp'])
                ->setCellValueExplicit('G' . $i, $element['ean'], DataType::TYPE_STRING)
                //->setCellValue('G' . $i, $element['ampel'])
                ->setCellValue('H' . $i, $element['gewicht'])
                //->setCellValue('I' . $i, $element['aktion'])
                ->setCellValue('I' . $i, $element['aktionende'])
                ->setCellValue('J' . $i, $element['auslaufartikel'])
                ->setCellValue('K' . $i, $element['preisneu'])
                ->setCellValue('L' . $i, $element['preisneuUvp']);
            $sheet->getStyle('D'.$i)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
            $sheet->getStyle('E'.$i)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
            $sheet->getStyle('F'.$i)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
            $sheet->getStyle('K'.$i)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
            $sheet->getStyle('L'.$i)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
            if($sheet->getCell('D' . $i)->getFormattedValue()>0){

                $sheet->getStyle('E'.$i)->getFont()->setStrikethrough(1);
            }

            $i++;
        }
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);
        $writer->save(dirname(__DIR__) . '../../../../../export/excel/' . str_replace('/', '', $_GET['name']) . $_GET['user'] . '.xlsx');

        $file = dirname(__DIR__) . '../../../../../export/excel/' . str_replace('/', '', $_GET['name']) . $_GET['user'] . '.xlsx';

        header("Content-Description: File Transfer");
        header("Content-Type: application/octet-stream");
        header('Content-Transfer-Encoding: binary');
        header('Content-Disposition: attachment; filename="' . $_GET['name'] . $_GET['user'] . '.xlsx"');
        ob_clean();

        readfile($file);

        exit();

    }

}
