<?php

use OxidEsales\Eshop\Core\Registry;

require_once dirname(__FILE__).'/tcpdf/tcpdf.php';

class bn_pricelistpdf extends tcpdf
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        //
        $this->SetMargins(
            12.5,
            25.0,
            12.5,
            true
        );
        //
        $this->AddPage('LANDSCAPE','A4');
    }

    /**
     *
     */
    public function Header() {
        // Logo
        //$image_file = K_PATH_IMAGES.'logo_example.jpg';
        //$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font

        $this->SetTopMargin(10);


        $this->SetFont($this->getFontFamily(), 'B', 12, 'default', false);

        $this->Cell(120, 6, $_GET['name'].' - '.Registry::getLang()->translateString("STAND"). ': '.date('d.m.Y'), '', 0, 'L');
        $this->Ln();

        $this->Cell(120, 6, '', '', 0, 'L');
        $this->Ln();


        //
        $this->setCellPaddings(1,1,1,1);
        $this->writeBnCell(Registry::getLang()->translateString('EXCEL_ARTNUM'), Registry::getLang()->translateString('EXCEL_NAME'), Registry::getLang()->translateString('EXCEL_VARNAME'),Registry::getLang()->translateString('EXCEL_PRICEACTION'), Registry::getLang()->translateString('EXCEL_PRICE'), Registry::getLang()->translateString('EXCEL_UVP'), Registry::getLang()->translateString('EXCEL_EAN'),Registry::getLang()->translateString('EXCEL_AMPEL'),Registry::getLang()->translateString('EXCEL_GEWICHT'),Registry::getLang()->translateString('EXCEL_AKTION'),Registry::getLang()->translateString('EXCEL_AUSLAUFARTIKEL'),Registry::getLang()->translateString('EXCEL_AKTIONENDE'));


        //$this->SetFont($this->getFontFamily(), '');
    }


    /**
     * @param $strName
     * @param $strArtnum
     * @param $strUvpPreis
     * @param $strEkPreis
     */
    public function writeBnCell($strArtnum, $strName, $strVarName, $strActionPreis , $strUvpPreis, $strEkPreis, $strEAN,$strAmpel,$strGewicht,$strAktion,$strAuslauf,$strAktionende)
    {



        $this->SetFont($this->getFontFamily(), '', 9, 'default', false);

        //$this->Cell(40, 6, $strVarName, 'TLRB', 0, 'L', false,'', 3);
        $this->Cell(17, 6, $strArtnum, 'TLRB', 0, 'L', false,'', 3);

        if(strlen($strName)>100){
            $this->SetFont($this->getFontFamily(), '', 6, 'default', false);
            $this->Cell(90, 6, $strName, 'TLRB', 0, 'L', false, '', 3);

        }
        else if(strlen($strName)>80){
            $this->SetFont($this->getFontFamily(), '', 7, 'default', false);
            $this->Cell(90, 6, $strName, 'TLRB', 0, 'L', false, '', 3);

        } else if(strlen($strName)>50) {
            $this->SetFont($this->getFontFamily(), '', 8, 'default', false);
            $this->Cell(90, 6, $strName, 'TLRB', 0, 'L', false, '', 3);

        } else {
            $this->SetFont($this->getFontFamily(), '', 9, 'default', false);
            $this->Cell(90, 6, $strName, 'TLRB', 0, 'L', false, '', 3);

        }


        if(strlen($strVarName)>30){
            $this->SetFont($this->getFontFamily(), '', 6, 'default', false);
            $this->Cell(40, 6, $strVarName, 'TLRB', 0, 'L', false, '', 3);

        }
        else if(strlen($strVarName)>20){
            $this->SetFont($this->getFontFamily(), '', 7, 'default', false);
            $this->Cell(40, 6, $strVarName, 'TLRB', 0, 'L', false, '', 3);

        } else if(strlen($strVarName)>10) {
            $this->SetFont($this->getFontFamily(), '', 8, 'default', false);
            $this->Cell(40, 6, $strVarName, 'TLRB', 0, 'L', false, '', 3);

        } else {
            $this->SetFont($this->getFontFamily(), '', 9, 'default', false);
            $this->Cell(40, 6, $strVarName, 'TLRB', 0, 'L', false, '', 3);

        }
        $this->SetFont($this->getFontFamily(), '', 9, 'default', false);

        $this->Cell(12, 6, str_replace('.', ',', $strActionPreis), 'TLRB', 0, 'R', false,'', 3);
        if($strActionPreis>0){

        $this->SetFont($this->getFontFamily(),"D");
        }
        $this->Cell(12, 6, str_replace('.', ',', $strUvpPreis), 'TLRB', 0, 'R', false,'', 3);
        $this->SetFont($this->getFontFamily(),"");

        $this->Cell(12, 6, str_replace('.',',',$strEkPreis), 'TLRB', 0, 'R', false,'', 3);
        $this->Cell(25, 6, $strEAN, 'TLRB', 0, 'R', false,'', 3);
        //$this->Cell(20, 6, $strAmpel, 'TLRB', 0, 'R', false,'', 3);
        $this->Cell(12, 6, $strGewicht, 'TLRB', 0, 'R', false,'', 3);
        //$this->Cell(12, 6, $strAktion, 'TLRB', 0, 'R', false,'', 3);
        if($strAuslauf == 'Will be discontinued'){
            $this->SetFont($this->getFontFamily(), '', 7, 'default', false);
        }
        $this->Cell(20, 6, $strAuslauf, 'TLRB', 0, 'R', false,'', 3);
        $this->SetFont($this->getFontFamily(), '', 9, 'default', false);

        $this->Cell(20, 6, $strAktionende, 'TLRB', 0, 'R', false,'', 3);
        $this->Ln();
    }

    /**
     *
     */
    public function AddPage($orientation = '', $format = '', $keepmargins = false, $tocpage = false)
    {
        parent::AddPage($orientation, $format, $keepmargins, $tocpage);
        //
        $this->SetTopMargin(29);
    }

    /**
     * @param $strHtml
     */

    public function setHeaderHtml($strHtml)
    {
        $this->strHeaderHtml = $strHtml;
    }
}
