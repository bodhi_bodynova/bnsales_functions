<?php

namespace Bodynova\bnSales_Functions\Application\Controller;

use OxidEsales\Eshop\Core\Registry;

class bn_AccountPasswordController extends bn_AccountPasswordController_parent{
    public function render()
    {
        if ($_GET['forcechangepassword']) {
            Registry::getUtilsView()->addErrorToDisplay(Registry::getLang()->translateString('PLEASE_CHANGE_PASSWORD'), false, true);
        }
        return parent::render();
    }
}