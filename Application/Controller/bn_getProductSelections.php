<?php

namespace Bodynova\bnSales_Functions\Application\Controller;

use OxidEsales\Eshop\Core\Registry;

class bn_getProductSelections extends \OxidEsales\Eshop\Application\Controller\FrontendController{
    // teplate for the view
    protected $_sThisTemplate = 'bn_selections.tpl';

    /**
     *
     */
    public function getList()
    {
        //
        $this->_aViewData['oxid'] = $_GET['oxid'];

        //
        $myConfig = $this->getConfig();
        $myConfig->setConfigParam('blLoadVariants', true);


        #echo Registry::getLang();
        #die();


        //$artlist->loadByArticlesIDs($arrOXIDs)
        //
        $this->_oProduct = oxNew('oxarticle');
        $this->_oProduct->loadInLang($_GET['lang'], $_GET['oxid']);


        //
        $arrSelections = array();
        $arrVariants = $this->_oProduct->getvariants();

        /*
        foreach ($arrVariants AS $oxid => $objVariant) {
            //
            #echo '<pre>';
            #print_r($objVariant);
            #die;
            $arrSelections[$oxid] = array(
                'parent' => $_GET['oxid'],
                'oxid' => $oxid,
                'pic' => $objVariant->oxarticles__oxpic1->value,
                'title' => $objVariant->oxarticles__oxvarselect->value,
                'artnum' => $objVariant->oxarticles__oxartnum->value,
                'price' => $objVariant->getPrice()->getNettoPrice(),
                'uvpprice' => $objVariant->getUVPPrice(),
                'deliveryDate' => $objVariant->getDeliveryDate(),
                'mindeltime' => $objVariant->oxarticles__oxmindeltime->value,
                'maxdeltime' => $objVariant->oxarticles__oxmaxdeltime->value,
                'delunit' => $objVariant->oxarticles__oxdeltimeunit->value,
                'bnstockflag' => $objVariant->oxarticles__bnflagbestand->value,
                'bncssflag' => $objVariant->showCssFlag(),
                'veEinheit' => $objVariant->oxarticles__verpackungseinheit->value,
                'preisneu' => $objVariant->getPreisNeu(),
                'bnflagrecall'  => $objVariant->oxarticles__bnflagrecall->value,
                'recallreason'  => $objVariant->oxarticles__recallreason->value,
                'recallreason_1'  => $objVariant->oxarticles__recallreason_1->value,
                'recallreason_2'  => $objVariant->oxarticles__recallreason_2->value,
                'aktionsende'   => $objVariant->oxarticles__aktionsende->value,
                'bnflagauslaufartikel' => $objVariant->oxarticles__bnflagauslaufartikel->value,
                'isAktionbeendet' => $objVariant->isAktionbeendet(),
                'oxpricea_aktionspreis' => $objVariant->oxarticles__oxpricea_aktionspreis->value,
                'oxpriceb_aktionspreis' => $objVariant->oxarticles__oxpriceb_aktionspreis->value,
                'oxpricec_aktionspreis' => $objVariant->oxarticles__oxpricec_aktionspreis->value,
                'oxpriced_aktionspreis' => $objVariant->oxarticles__oxpriced_aktionspreis->value,
                'oxpricee_aktionspreis' => $objVariant->oxarticles__oxpricee_aktionspreis->value,
                'oxpricef_aktionspreis' => $objVariant->oxarticles__oxpricef_aktionspreis->value,
                'oxpricea_aktionsbeginn' => $objVariant->oxarticles__oxpricea_aktionsbeginn->value,
                'oxpriceb_aktionsbeginn' => $objVariant->oxarticles__oxpriceb_aktionsbeginn->value,
                'oxpricec_aktionsbeginn' => $objVariant->oxarticles__oxpricec_aktionsbeginn->value,
                'oxpriced_aktionsbeginn' => $objVariant->oxarticles__oxpriced_aktionsbeginn->value,
                'oxpricee_aktionsbeginn' => $objVariant->oxarticles__oxpricee_aktionsbeginn->value,
                'oxpricef_aktionsbeginn' => $objVariant->oxarticles__oxpricef_aktionsbeginn->value,
                'oxpricea_aktionsende' => $objVariant->oxarticles__oxpricea_aktionsende->value,
                'oxpriceb_aktionsende' => $objVariant->oxarticles__oxpriceb_aktionsende->value,
                'oxpricec_aktionsende' => $objVariant->oxarticles__oxpricec_aktionsende->value,
                'oxpriced_aktionsende' => $objVariant->oxarticles__oxpriced_aktionsende->value,
                'oxpricee_aktionsende' => $objVariant->oxarticles__oxpricee_aktionsende->value,
                'oxpricef_aktionsende' => $objVariant->oxarticles__oxpricef_aktionsende->value
            );

            #echo '<pre>';
            #print_r($arrSelections);
            #echo '</pre>';
            #die;

        */
            //
            #$objProduct = oxNew('oxarticle');
            #$objProduct->load($oxid);
            #$arrSelectLists = $objProduct->getSelectLists();

            //
            #if (count($arrSelectLists)) {
             #   $arrSelections[$oxid]['lists'] = $arrSelectLists;
            #}
            // ende
        #}
        //
        $this->_aViewData['arrSelections'] = $arrSelections;
        $this->_aViewData['arrVariants'] = $arrVariants;
        $this->_aViewData['mylang'] = $_GET['lang'];
        // ende
    }
}