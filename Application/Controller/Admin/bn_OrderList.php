<?php

namespace Bodynova\bnSales_Functions\Application\Controller\Admin;

class bn_OrderList extends bn_OrderList_parent{
    public function render()
    {
        parent::render();
        return 'bn_order_list.tpl';
    }
}