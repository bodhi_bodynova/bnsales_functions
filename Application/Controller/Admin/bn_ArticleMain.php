<?php

namespace Bodynova\bnSales_Functions\Application\Controller\Admin;

class bn_ArticleMain extends bn_ArticleMain_parent{
    public function render()
    {
        parent::render();
        return 'bn_article_main.tpl';
    }
}