<?php

namespace Bodynova\bnSales_Functions\Application\Controller\Admin;

class bn_OrderOverview extends bn_OrderOverview_parent{
    public function render()
    {
        parent::render();
        return 'bn_order_overview.tpl';
    }
}