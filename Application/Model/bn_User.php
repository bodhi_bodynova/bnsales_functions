<?php
namespace Bodynova\bnSales_Functions\Application\Model;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;

class bn_User extends bn_User_parent{

    public $test = 'test';
    protected $arFavorites = array();
    protected $arTest2 = array();

    protected $iswhitelabel = null;

    /**
     * Class constructor, initiates parent constructor (parent::oxBase())
     */
    public function __construct()
    {
        parent::__construct();


        if (!$this->hasUserFavorites($this->getId())) {
            $this->arFavorites = $this->getUserFavorites($this->getId());
        }

        $this->init('oxuser');
    }

    /**
     * gibt die User Eingabe (Layout Checkbox) Neutraler Versand wieder
     * @return |null
     */
    public function getIsWhiteLabel()
    {
        return $this->iswhitelabel;
    }

    /**
     * setzt die User Eingabe (Layout Checkbox) Neutraler Versand
     * @param $wert
     */
    public function setIsWhiteLabel($wert)
    {
        $this->iswhitelabel = $wert;
    }

    /**
     * @param null $sOXID
     * @return bool
     */
    public function hasUserFavorites($sOXID = null)
    {
        // check User OXID
        if (!$sOXID) {
            $sOXID = $this->getId();
        }
        // if not set Favorites, try to
        if (!count($this->arFavorites) > 0) {
            $this->arFavorites = $this->getUserFavorites($sOXID);
        }
        // return bool
        if (count($this->arFavorites) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param null $sOXID
     * @return array|mixed
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseErrorException
     */
    public function getUserFavorites($sOXID = null)
    {
        if (count($this->arFavorites) === 0) {
            if (!$sOXID) {
                $sOXID = $this->getId();
            }
            $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
            $ssql = 'select * from favoriten where oxuserid = ' . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($sOXID) . ' order by sort asc';
            $result = $oDb->getAll($ssql);

            if (count($result) === 0) {
                $this->arFavorites = $result;
                //echo count($oDb->getAll($ssql));
                //echo 'empty array';
            } else {
                $this->arFavorites = $result;
            }
        }

        return $this->arFavorites;
    }


    /**
     * @param $arr
     */
    public function setUserFavorites($arr = null, $sOXID = null)
    {

        if (!$sOXID) {
            $sOXID = $this->getId();
        }
        $response = null;
        // alle User Favoriten löschen.
        if (count($this->getUserFavorites($sOXID)) > 0) {

            $oDB = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
            $ssql = "delete from favoriten where oxuserid='" . $sOXID . "' ";
            $oDB->execute($ssql);

            if (count($arr) > 0) {

                foreach ($arr as $key) {
                    $oDB = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
                    $ssql = "insert into favoriten ( oxid, oxactive, oxuserid, favid, favtype, sort , text) values ( '" . $this->generateUId() . "', '" . $key[1] . "', '" . $key[2] . "', '" . $key[3] . "', '" . $key[4] . "', '" . $key[5] . "', '" . $key[6] . "')";
                    $oDB->execute($ssql);
                }

            }
        } else {

            if (count($arr) > 0) {

                foreach ($arr as $value => $key) {
                    $oDB = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
                    $ssql = "insert into favoriten ( oxid, oxactive, oxuserid, favid, favtype, sort , text) values ( '" . $this->generateUId() . "', '" . $key[1] . "', '" . $key[2] . "', '" . $key[3] . "', '" . $key[4] . "', '" . $key[5] . "', '" . $key[6] . "')";
                    $oDB->execute($ssql);
                }

            }
        }
        $response = $ssql;

        /*
        $oDB = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        $ssql = "insert into favoriten ( oxid, oxactive, oxuserid, favid, favtype, sort) values ( '".$this->generateUId()."', '".$arr[1]."', '".$arr[2]."', '".$arr[3]."', '".$arr[4]."', '".$arr[5]."')";
        $oDB->execute($ssql);
        */

        return $response;
    }


    /**
     * Oxid UUID erstellen.
     * @return string
     */
    public function generateUId()
    {
        return substr(md5(uniqid('', true) . '|' . microtime()), 0, 32);
    }

    public function setStandardFavorites()
    {

    }

    /**
     * TODO: Funktion zur Bestimmung der Usergruppe. Abhängig davon werden Aktionspreise, Händlerpreise, Aktionszeiträume zurückgegeben...
     */

    public function getUserPriceData(){
        $query = 'SELECT OXID,OXGROUPSID FROM oxobject2group WHERE OXOBJECTID = "' . $this->getID() . '"';
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        try{
            $result = $oDb->getAll($query);
        } catch(\Exception $e){
            echo 'Error : ' . $e->getMessage() . "\n";
        }
        foreach($result as $key){
            if(stristr($key['OXGROUPSID'],'oxidprice')){
                return substr($key['OXGROUPSID'],-1);
            }
        }
    }

    /**
     * VATPRÜFUNG AUSKOMMENTIERT
     * Performs bunch of checks if user profile data is correct; on any
     * error exception is thrown
     *
     * @param string $sLogin      user login name
     * @param string $sPassword   user password
     * @param string $sPassword2  user password to compare
     * @param array  $aInvAddress array of user profile data
     * @param array  $aDelAddress array of user profile data
     *
     * @throws oxUserException, oxInputException
     */
    public function checkValues($sLogin, $sPassword, $sPassword2, $aInvAddress, $aDelAddress)
    {
        /** @var \OxidEsales\Eshop\Core\InputValidator $oInputValidator */
        $oInputValidator = Registry::getInputValidator();

        // 1. checking user name
        $sLogin = $oInputValidator->checkLogin($this, $sLogin, $aInvAddress);

        // 2. checking email
        $oInputValidator->checkEmail($this, $sLogin);

        // 3. password
        $oInputValidator->checkPassword($this, $sPassword, $sPassword2, ((int) Registry::getConfig()->getRequestParameter('option') == 3));

        // 4. required fields
        $oInputValidator->checkRequiredFields($this, $aInvAddress, $aDelAddress);

        // 5. country check
        $oInputValidator->checkCountries($this, $aInvAddress, $aDelAddress);

        // 6. vat id check.

        /*
        try {
            $oInputValidator->checkVatId($this, $aInvAddress);
        } catch (\OxidEsales\Eshop\Core\Exception\ConnectionException $e) {
            // R080730 just oxInputException is passed here
            // if it oxConnectionException, it means it could not check vat id
            // and will set 'not checked' status to it later
        }*/

        // throwing first validation error
        if ($oError = Registry::getInputValidator()->getFirstValidationError()) {
            throw $oError;
        }
    }



}