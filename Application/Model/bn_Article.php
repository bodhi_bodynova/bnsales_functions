<?php

namespace Bodynova\bnSales_Functions\Application\Model;

use OxidEsales\Eshop\Core\DatabaseProvider;

class bn_Article extends bn_Article_parent
{
    protected $isFavorite = null;
    protected $preisneu = null;
    protected $preisaktuell = null;
    protected $boolVarAktion = null;
    protected $liefertage = 3;
    protected $aktionsbeginn = null;
    protected $aktionsende = null;

    public function getAktionsbeginn(){
        return $this->aktionsbeginn;
    }

    public function getAktionsende(){
        return $this->aktionsende;
    }

    /**
     * Returns true if article is not buyable
     *
     * @return bool
     */
    public function isBnNotBuyable()
    {
        if (date('Y.m.d', strtotime($this->oxarticles__oxdelivery->value) - (60 * 60 * 56)) >= date('Y.m.d')) {
            return false;
        }
        if($this->oxarticles__bnflagbestand->value == 2){
            return true;
        }
        return $this->_blNotBuyable;
    }

    /**
     * Checks if article is buyable.
     *
     * @return bool
     */
    public function isBnBuyable()
    {
        $lieferzeitsekunden = $this->liefertage * 24 * 60 * 60;
        /**
         * Wenn das Feld oxdelivery gefüllt ist,
         * und oxdelivery größer als das aktuelle Datum
         */
        if($this->oxarticles__oxdelivery->value !== '0000-00-00' && date('Y.m.d', strtotime($this->oxarticles__oxdelivery->value)) >= date('Y.m.d') ){
            /**
             * Wenn das oxdelivery Datum - Tage kleiner ist als das aktuelle Datum...
             * TRUE
             */
            if (date('Y.m.d', strtotime($this->oxarticles__oxdelivery->value) - $lieferzeitsekunden) <= date('Y.m.d')) {
                return true;
            }
        }
        /**
         * Wenn das Feld bnflagbestand auf 2 steht.
         * FALSE
         */
        if ($this->oxarticles__bnflagbestand->value == 2) {
            return false;
        }
        /**
         * Shop Standartüberprüfung:
         */
        return !($this->_blNotBuyableParent || $this->_blNotBuyable);
    }


    public function getMinVar($preisgruppe){
        $vars = $this->getFullVariants();
        /*
        $min = 9999999999;
        $preisAktion = 'oxarticles__oxprice' . $preisgruppe;
        $preis = 'oxarticles__oxprice'.$preisgruppe.'_aktionspreis';
        $normalLOWERTHANAktion = false;
        foreach($vars as $key){
            if(0<$key->$preis->value&&$key->$preis->value < $min){
                $min = $key->$preis->value;
                $normalLOWERTHANAktion = true;
            }
            if(0<$key->$preisAktion->value &&$key->$preisAktion->value < $min){
                $min = $key->$preisAktion->value;
                $normalLOWERTHANAktion = false;
            }
        }
        $this->boolVarAktion = $normalLOWERTHANAktion;
        return strval($min);
        */
        $minStreichpreis = 99999999;
        $minPreis = 99999999;

        $sStreichpreis = 'oxarticles__oxprice'.$preisgruppe.'_aktionspreis';
        $sPreis = 'oxarticles__oxprice' . $preisgruppe;
        foreach($vars as $key){
            if($key->$sStreichpreis->value > 0 && $key->$sStreichpreis->value < $minStreichpreis){
                $minStreichpreis = $key->$sStreichpreis->value;
            }
            if($key->$sPreis->value > 0 && $key->$sPreis->value < $minPreis){
                $minPreis = $key->$sPreis->value;
            }
        }

        if($minStreichpreis !=  99999999 && $minPreis < $minStreichpreis){
            return strval($minPreis);
        } else {
            return strval($minStreichpreis);
        }
        //return strval($min);
    }

    /**
     * Prüft, ob ein Variantenpreis niedriger ist als der niedrigste Aktionspreis
     * @return |null
     */
    public function boolVarAktionFunc(){
        return $this->boolVarAktion;
    }

    public function getPreisNeu()
    {
        switch ($this->_getUserPriceSufix()) {
            case 'a':
                $this->preisneu = $this->oxarticles__preisneua->value;
                break;
            case 'b':
                $this->preisneu = $this->oxarticles__preisneub->value;
                break;
            case 'c':
                $this->preisneu = $this->oxarticles__preisneuc->value;
                break;
            case 'd':
                $this->preisneu = $this->oxarticles__preisneud->value;
                break;
            case 'e':
                $this->preisneu = $this->oxarticles__preisneue->value;
                break;
            case 'f':
                $this->preisneu = $this->oxarticles__preisneue->value;
                break;
        }

        return $this->preisneu;
    }

    public function getUpdatePreis()
    {
        switch ($this->_getUserPriceSufix()) {
            case 'a':
                $this->preisupdate = $this->oxarticles__preisneua->value;
                break;
            case 'b':
                $this->preisupdate = $this->oxarticles__preisneub->value;
                break;
            case 'c':
                $this->preisupdate = $this->oxarticles__preisneuc->value;
                break;
            case 'd':
                $this->preisupdate = $this->oxarticles__preisneud->value;
                break;
            case 'e':
                $this->preisupdate = $this->oxarticles__preisneue->value;
                break;

        }

        return $this->preisupdate;
    }


    public function getPreisAktuell()
    {

        switch ($this->_getUserPriceSufix()) {
            case 'a':
                $this->preisaktuell = $this->oxarticles__oxpricea->value;
                break;
            case 'b':
                $this->preisaktuell = $this->oxarticles__oxpriceb->value;
                break;
            case 'c':
                $this->preisaktuell = $this->oxarticles__oxpricec->value;
                break;
            case 'd':
                $this->preisaktuell = $this->oxarticles__oxpriced->value;
                break;
            case 'e':
                $this->preisaktuell = $this->oxarticles__oxpricee->value;
                break;
            case 'f':
                $this->preisaktuell = $this->oxarticles__oxpricef->value;
                break;
        }

        return $this->preisaktuell;
    }

    /**
     * Return price suffix
     *
     * @return null
     */
    public function _getUserPriceSufix()
    {
        $sPriceSufix = '';
        $oUser = $this->getArticleUser();

        if ($oUser) {
            if ($oUser->inGroup('oxidpricea')) {
                $sPriceSufix = 'a';
            } elseif ($oUser->inGroup('oxidpriceb')) {
                $sPriceSufix = 'b';
            } elseif ($oUser->inGroup('oxidpricec')) {
                $sPriceSufix = 'c';
            } elseif ($oUser->inGroup('oxidpriced')) {
                $sPriceSufix = 'd';
            } elseif ($oUser->inGroup('oxidpricee')) {
                $sPriceSufix = 'e';
            } elseif ($oUser->inGroup('oxidpricef')) {
                $sPriceSufix = 'f';
            }
        }

        # echo '$sPriceSufix: '.$sPriceSufix.'<br>';

        return $sPriceSufix;
    }

    /**
     * Favorite Handling
     */
    public function isFavorite()
    {
        $user = $this->getUser();
        if ($this->isFavorite === null) {
            $strSQL = 'select 1 from oxfavorites WHERE OXUSER=\'' . $user->oxuser__oxid->value . '\' AND OXARTICLE=\'' . $this->getId() . '\'';
            $this->isFavorite = (bool)\OxidEsales\Eshop\Core\DatabaseProvider::getDb()->getOne($strSQL);
        }
        return $this->isFavorite;
    }

    /**
     *
     */
    public function getUVPPrice()
    {
        return $this->oxarticles__oxprice->value;
    }

    /**
     * holt die Lieferzeit vom Parentartikel
     */
    public function getParentLieferzeit()
    {

        $arvariants = $this->_getVariantsIds();
        $amindeltime = $this->getmindeltime();
        $amaxdeltime = $this->getaxdeltime();
        $mindeltime = $amindeltime[0];
        $maxdeltime = $amaxdeltime[0];

        /*
        echo '<pre>';
        print_r($amaxdeltime);
        echo '</pre>';
        */

        // OXID = $this->oxarticles__oxid->value;

        if ($amaxdeltime[0] > 0) {
            return "$mindeltime" . " - " . "$maxdeltime";
        }
        return false;

    }


    /**
     * holt die Mindestlieferzeit
     * @return array
     */
    public function getmindeltime()
    {
        $aSelect = array();
        if (($sId = $this->getId())) {
            $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(\OxidEsales\Eshop\Core\DatabaseProvider::FETCH_MODE_ASSOC);
            $sQ = "select oxmindeltime from " . $this->getViewName(true) . " where oxparentid = " . $oDb->quote($sId) . " and " .
                $this->getSqlActiveSnippet(true) . " order by oxmindeltime";
            $oRs = $oDb->select($sQ);
            if ($oRs != false && $oRs->count() > 0) {
                while (!$oRs->EOF) {
                    $aSelect[] = reset($oRs->getFields());
                    $oRs->fetchRow();
                }
            }
        }
        return $aSelect;
    }

    /**
     * holt die Maximale Lieferzeit
     * @return array
     */
    public function getmaxdeltime()
    {
        $aSelect = array();
        if (($sId = $this->getId())) {
            $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(\OxidEsales\Eshop\Core\DatabaseProvider::FETCH_MODE_ASSOC);
            $sQ = "select oxmaxdeltime from " . $this->getViewName(true) . " where oxparentid = " . $oDb->quote($sId) . " and " .
                $this->getSqlActiveSnippet(true) . " order by oxmaxdeltime DESC";
            $oRs = $oDb->select($sQ);
            if ($oRs != false && $oRs->count() > 0) {
                while (!$oRs->EOF) {
                    $aSelect[] = reset($oRs->getFields());
                    $oRs->fetchRow();
                }
            }
        }
        return $aSelect;
    }

    /**
     * gibt die maximale Lieferdauer als String zurück
     * @return mixed
     */
    public function getmaxdeltimeS()
    {
        if ($this->getmaxdeltime()) {
            $a = $this->getmaxdeltime();
            return $a[0];
            #print_r($this->getmaxdeltime());
        }
    }

    /**
     * gibt die mindest Lieferdauer als String zurück
     * @return mixed
     */
    public function getmindeltimeS()
    {
        if ($this->getmindeltime()) {
            $a = $this->getmindeltime();
            return $a[0];
            #print_r($this->getmindeltime());
        }
    }

    /**
     * gibt den Wert von bnFlagStatus zurück
     * @return mixed
     */
    public function getBnFlag()
    {
        // 0 = grün
        // 1 = gelb
        // 2 = rot
        return $this->oxarticles__bnflagbestand->value;
    }

    /**
     * Ermittelt die StockFlag Farbe und gibt die Css Klasse zurück
     * @return null|string
     */
    public function showCssFlag()
    {
        // Farbe ermitteln und CSS Klasse zurück geben:
        $flag = $this->getBnFlag();
        $result = null;
        switch ($flag) {
            case 2:
                $result = "notOnStock";
                break;
            case 1:
                $result = "lowStock";
                break;
            default:
                $result = "onStock";
                break;
        }
        return $result;
    }

    /**
     * Gibt als String zurück ob das Produkt Varianten hat
     * @return null|string
     */
    public function getProductStatus()
    {
        $ergebnis = null;
        if ($this->getVariantsCount() > 0) {
            $ergebnis = "hat Varianten";
        } else {
            $ergebnis = "hat keine Varianten";
        }
        return $ergebnis;
    }

    /**
     * Returns formatted delivery date. If the date is not set ('0000-00-00') returns false.
     *
     * @return string | bool
     */
    public function getDeliveryDate()
    {

        if ($this->oxarticles__oxdelivery->value != '0000-00-00') {
            return $this->oxarticles__oxdelivery->value;
        }
        return false;
        
        if ($this->oxarticles__oxdelivery->value != '0000-00-00') {
            return \OxidEsales\Eshop\Core\Registry::get("oxUtilsDate")->formatDBDate($this->oxarticles__oxdelivery->value);
        }
        #return $this->oxarticles__oxdelivery->value;
        return false;
    }

    /**
     * Falls Streichpreis(und somit Aktionspreis), gebe Streichpreis zurück und setzte Aktionsbeginn/Ende
     * @return mixed
     */
    public function getUserStreichpreis(){
        $suff = $this->_getUserPriceSufix();
        $preis = 'oxarticles__oxprice' . $suff;
        $aktionspreis = 'oxarticles__oxprice'.$suff.'_aktionspreis';
        $aktionsbeginn = 'oxarticles__oxprice'.$suff.'_aktionsbeginn';
        $aktionsende = 'oxarticles__oxprice'.$suff.'_aktionsende';
        $this->aktionsbeginn = $this->$aktionsbeginn->value;
        $this->aktionsende = $this->$aktionsende->value;
        if( $this->$aktionspreis->value !== 0
            && $this->$aktionspreis->value > $this->$preis->value
            && date('Y.m.d', strtotime($this->$aktionsende->value)) >= date('Y.m.d')
            && date('Y.m.d', strtotime($this->$aktionsbeginn->value)) < date('Y.m.d') ){
            $this->aktionspreis = $this->$aktionspreis->value;
        } else {
            $this->aktionspreis = 0;
        }

        return $this->aktionspreis;
    }

    public function getPicture(){
        $odb = DatabaseProvider::getDb();
        $select = 'SELECT OXPIC1 from oxarticles WHERE OXID = ?';
        $pic = $odb->getOne($select,array($this->oxarticles__oxid->value));
        if ($pic === '') {
            return '';
        }else{
            return $this->oxarticles__oxpic1->value;
        }
    }

    public function alertInList(){

        $oDb = DatabaseProvider::getDb();
        $oUser = $this->getArticleUser();

        if(!$oUser->oxuser__oxpassword->value){
            return 0;
        }

        $oxid = $this->oxarticles__oxid->value;

        $select = 'SELECT OXID FROM oxarticles2useralert WHERE OXARTICLEID = ? AND OXUSERID = ?';
        $res = $oDb->getAll($select,array($oxid,$oUser->getId()));
        if($res){
            return 1;
        } else return 0;
    }

}
