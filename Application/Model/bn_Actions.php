<?php

namespace Bodynova\bnSales_Functions\Application\Model;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\UtilsFile;

class bn_Actions extends bn_Actions_parent{
    /**
     * Returns assigned banner article picture url
     *
     * @return string
     */
    public function getBannerPictureUrl()
    {
        if (isset($this->oxactions__oxpic) && $this->oxactions__oxpic->value) {

            $sPromoDir = Registry ::get("oxUtilsFile")->normalizeDir(UtilsFile::PROMO_PICTURE_DIR);

            $sPath = "https://bodynova.de/out/pictures/" . $sPromoDir . $this->oxactions__oxpic->value;

            #return $this->getConfig()->getPictureUrl( $sPromoDir.$this->oxactions__oxpic->value, false );
            return $sPath;
        }
    }
}