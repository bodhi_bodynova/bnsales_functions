<?php

namespace Bodynova\bnSales_Functions\Application\Model;

class bn_Address extends bn_Address_parent
{
    public function filterAdresses($strSearch)
    {
        //
        $oAdressList = oxNew("oxlist");
        $oAdressList->init("oxbase");
        //
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        /*
        $sSelect  = "select * from oxaddress ";
        $sSelect .= "where OXUSERID='".$this->getUser()->getId()."' ";
        $sSelect .= "AND MATCH (`OXCOMPANY`, `OXFNAME`, `OXLNAME`, `OXCITY`) AGAINST (".$oDb->quote($strSearch.'*')." IN BOOLEAN MODE)";
        */
        $sSelect = "select a.* from oxaddress as a where a.OXUSERID = '" . $this->getUser()->getId() . "' AND a.OXID IN(SELECT OXID FROM oxaddress WHERE  OXCOMPANY LIKE " . $oDb->quote('%' . $strSearch . '%') . " OR OXFNAME LIKE " . $oDb->quote('%' . $strSearch . '%') . " OR OXSTREET LIKE " . $oDb->quote('%' . $strSearch . '%') . " OR OXZIP LIKE " . $oDb->quote('%' . $strSearch . '%') . " OR OXLNAME LIKE " . $oDb->quote('%' . $strSearch . '%') . " OR OXADDINFO LIKE " . $oDb->quote('%' . $strSearch . '%') . " OR OXCITY LIKE " . $oDb->quote('%' . $strSearch . '%') . ")";
        //
        //print_r($sSelect);
        //die();
        $oAdressList->selectString($sSelect);
        //
        return $oAdressList;
        // ende
    }
}