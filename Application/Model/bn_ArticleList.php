<?php

namespace Bodynova\bnSales_Functions\Application\Model;

class bn_ArticleList extends \OxidEsales\Eshop\Application\Model\ArticleList {
    /**
     * @param $arrOXIDs
     */
    public function loadByArticlesIDs($arrOXIDs)
    {
        //
        $sArticleTable = getViewName('oxarticles');
        //
        $sSelect  = "select * from $sArticleTable ";
        $sSelect .= "where ".$this->getBaseObject()->getSqlActiveSnippet()." and $sArticleTable.OXID IN('".implode("','", $arrOXIDs)."') ";
        #$sSelect .= "order by $sArticleTable.OXTITLE_1 desc";
        //

        $this->selectString($sSelect);
        // ende
    }

    public function output(){
        echo "TEST";
    }
    /**
     * Loads the article list by orders ids
     *
     * @param array $aOrders user orders array
     *
     * @return null;
     */

    public function loadOrderArticles($aOrders)
    {
        if(is_object($aOrders)){
            if (!count($aOrders)) {
                $this->clear();

                return;
            }

            foreach ($aOrders as $iKey => $oOrder) {
                $aOrdersIds[] = $oOrder->getId();
            }
        } else {
            $aOrdersIds = $aOrders;
        }



        $oBaseObject = $this->getBaseObject();
        $sArticleTable = $oBaseObject->getViewName();
        $sArticleFields = $oBaseObject->getSelectFields();
        $sArticleFields = str_replace("`$sArticleTable`.`oxid`", "`oxorderarticles`.`oxartid` AS `oxid`", $sArticleFields);

        $sSelect = "SELECT $sArticleFields FROM oxorderarticles ";
        $sSelect .= "left join $sArticleTable on oxorderarticles.oxartid = $sArticleTable.oxid ";
        $sSelect .= "WHERE oxorderarticles.oxorderid IN ( '" . implode("','", $aOrdersIds) . "' ) ";
        $sSelect .= "order by $sArticleTable.oxid ";



        $this->selectString($sSelect);

        // not active or not available products must not have button "tobasket"
        $sNow = date('Y-m-d H:i:s');
        foreach ($this as $oArticle) {
            if (!$oArticle->oxarticles__oxactive->value &&
                ($oArticle->oxarticles__oxactivefrom->value > $sNow ||
                    $oArticle->oxarticles__oxactiveto->value < $sNow
                )
            ) {
                $oArticle->setBuyableState(false);
            }
        }
    }
}
