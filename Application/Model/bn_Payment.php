<?php

namespace Bodynova\bnSales_Functions\Application\Model;

use OxidEsales\Eshop\Core\Registry;

class bn_Payment extends bn_Payment_parent{

    public function init($tableName = null, $forceAllFields = false)
    {
        Registry::getUtils()->redirect(Registry::getConfig()->getShopHomeURL() . 'cl=order', false, 301);
    }
}