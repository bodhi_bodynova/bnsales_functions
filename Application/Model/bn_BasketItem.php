<?php

namespace Bodynova\bnSales_Functions\Application\Model;

use OxidEsales\Eshop\Core\Registry;

class bn_BasketItem extends bn_BasketItem_parent{

    public $sort = 0;
    protected $_artnum = null;
    protected $_ve = null;

    public function __construct()
    {
        parent::__construct();
    }



    public function getPic()
    {
        $oArticle = $this->getArticle();
        if($oArticle->oxarticles__oxpic1->value !== ''){
            return 'https://cdn.bodynova.de/out/pictures/generated/product/1/900_900_75/' . $oArticle->oxarticles__oxpic1->value;
        } else {
            return 'https://bodynova.de/out/imagehandler.php?artnum=' . $oArticle->oxarticles__oxartnum->value .'&size=900_900_75';
        }

    }

    public function getSort()
    {
        return $this->sort;
    }

    public function setSort($value)
    {
        $this->sort = $value;
    }

    /**
     * Returns product artnum
     *
     * @return string
     */
    public function getArtNum()
    {
        if ($this->_artnum === null) {

            $oArticle = $this->getArticle();
            $this->_artnum = $oArticle->oxarticles__oxartnum->value;
        }

        return $this->_artnum;
    }

    public function getVE()
    {
        if ($this->_ve === null) {

            $oArticle = $this->getArticle();
            $this->_ve = $oArticle->oxarticles__verpackungseinheit->value;
        }
        return $this->_ve;
    }

    public function getParentId()
    {
        $oArticle = $this->getArticle();
        if ($oArticle->getParentId() !== '') {
            return $oArticle->getParentId();
        }
        return $oArticle->oxarticles__oxid->value;
    }
}