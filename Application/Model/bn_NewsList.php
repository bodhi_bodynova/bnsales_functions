<?php

namespace Bodynova\bnSales_Functions\Application\Model;

class bn_NewsList extends bn_NewsList_parent{
    /**
     * Gibt die aktuellste News zurück.
     * @return array
     */
    public function getlastNews()
    {
        $myConfig = $this->getConfig();
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();

        $sNewsViewName = getViewName('oxnews');
        $oBaseObject = $this->getBaseObject();

        if ($oUser = $this->getUser()) {
            // performance - only join if user is logged in
            $sSelect = "select $sNewsViewName.`oxid`, $sNewsViewName.`oxshortdesc`, $sNewsViewName.`oxlongdesc`, $sNewsViewName.`oxdate`  from $sNewsViewName ";
            $sSelect .= "left join oxobject2group on oxobject2group.oxobjectid=$sNewsViewName.oxid where (";
            $sSelect .= "oxobject2group.oxgroupsid in ( select oxgroupsid from oxobject2group where oxobjectid='" . $oUser->getId() . "' ) and "; // or
            $sSelect .= "( oxobject2group.oxgroupsid is null ) )";
        } else {
            $sSelect = "select $sNewsViewName.`oxid`, $sNewsViewName.`oxshortdesc`, $sNewsViewName.`oxlongdesc`, $sNewsViewName.`oxdate` from $sNewsViewName ";
            $sSelect .= "left join oxobject2group on oxobject2group.oxobjectid=$sNewsViewName.oxid where (oxobject2group.oxgroupsid is null )";
        }
        $sSelect .= " and " . $oBaseObject->getSqlActiveSnippet() . " order by `oxdate` desc";
        //$sSelect .= " and ".$sNewsViewName.".oxactive = 1 order by `oxdate` desc";
        $news[] = $oDb->getAll($sSelect);
        //echo $sSelect;
        return $news[0][0];
    }
}