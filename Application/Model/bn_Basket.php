<?php

namespace Bodynova\bnSales_Functions\Application\Model;

/**
 * @param $a
 * @param $b
 * @return bool
 */
function customBasketSorting($a, $b)
{
    return $a->getTitle() > $b->getTitle();
}

function drecustomBasketSorting($a, $b)
{
    return $a->getSort() > $b->getSort();
}

class bn_Basket extends bn_Basket_parent{

    protected $iswhitelabel = null;
    protected $upfile = null;

    public function getIsWhiteLabel()
    {
        return $this->iswhitelabel;
    }

    public function setIsWhiteLabel($wert)
    {
        $this->iswhitelabel = $wert;
    }

    public function getUpfile()
    {
        return $this->upfile;
    }

    public function setUpfile($wert)
    {
        $this->upfile = $wert;
    }

    /**
     * Returns basket items array
     *
     * @return array
     */
    public function getContents()
    {
        return $this->_aBasketContents;
    }

    /**
     * @param $userid
     * @param $wk
     */
    public function setWkAnsicht()
    {
        if ($_POST) {
            $userid = $_POST['userid'];
            if ($_POST['wkAnsicht'] === 'erweitert') {
                $wk = '1';
            } else {
                $wk = '0';
            }
            $user = oxNew('oxUser');
            $user->load($userid);
            $user->oxuser__wkansicht->rawValue = $wk;

            /*
            echo '<pre>';
            print_r($user);
            die();
            */
            $user->save();
        }

    }

    public function sortneu()
    {
        usort($this->_aBasketContents, 'drecustomBasketSorting');
        return json_encode($this->_aBasketContents);
    }
}