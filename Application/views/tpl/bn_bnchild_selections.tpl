<script>/*$('.imagecolumn img').magnify();*/</script>
<form>

    <table class="tableselections table table-hover " border="0" style="margin-bottom: 0">
        <!--<thead>
            <th class="hidden-xs"></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </thead>-->
        [{foreach from=$arrSelections item=_aProduct}]
        <div class="d-xl-none">
            <hr>
            <div class="row">
                <div class="col-4">
                    <a href="[{*$_productLink*}]#" onclick="loadArticleDetails('[{$_aProduct.oxid}]')" class="viewAllHover glowShadow corners" title="[{$_aProduct.title}]">
                        <div class="pictureBoxVar">
                            <a href="[{*$_productLink*}]#" onclick="loadArticleDetails('[{$_aProduct.oxid}]')" class="viewAllHover glowShadow corners" title="[{$_aProduct.title}]">
                                <img class="lazy-img img-thumbnail" src="https://bodynova.de/out/imagehandler.php?artnum=[{$_aProduct.artnum}]&size=450_450_100" alt="[{$_aProduct.title}]">
                            </a>
                        </div>
                </div>
                <div class="col-8">
                    <div class="smallFont">
                        [{oxmultilang ident="ARTNR" suffix="COLON"}] [{$_aProduct.artnum}]
                    </div>
                </div>
                <div class="col-sm-4 col-5 offset-2 offset-sm-4" style="margin-top:5px;">
                    [{* Rückrufaktion *}]
                    [{if $_aProduct.bnflagrecall == 1}]<button style="margin-top:5px;height:38px; width:40px;margin-right:5px; margin-left:0 !important;" type="button" class="btn btn-outline-danger bntooltip" data-toggle="tooltip" data-placement="top" title="[{$_aProduct.recallreason}]"><i class="fa fa-exclamation-triangle"></i></button>[{/if}]

                    [{* Aktionsartikel *}]
                    [{if $_aProduct.isAktionbeendet }]<button style="margin-top:5px;height:38px; width:40px;margin-right:5px;margin-left:5px" type="button" class="btn btn-outline-success bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AKTIONBIS"}][{$_aProduct.aktionsende|date_format:"%d.%m.%Y"}]"><i class="fas fa-euro-sign"></i></button>[{/if}]

                    [{* Auslaufartikel *}]
                    [{if $_aProduct.bnflagauslaufartikel == 1}]<button style="margin-top:5px;height:38px; width:40px;margin-right:5px;margin-left:5px" type="button" class="btn btn-auslauf btn-outline-warning bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AUSLAUFARTIKEL"}]"><i class="fa fa-exclamation-triangle"></i></button>[{/if}]
                </div>
                <div class="col-sm-4 col-5" [{if $_aProduct->isBnBuyable()}] style="margin-top:5px"[{/if}]>
                    <div class="text-right">
                        [{if $_aProduct->isBnBuyable()}]
                        [{*if $_aProduct->isBnBuyable()*}]
                            <input class="form-control amount" type="numeric" name="amountSmall[[{$_aProduct.oxid}]]" onchange="updateAmount(this,'amountSmall[[{$_aProduct.oxid}]]')" value="0" style="text-align: right;width:50px; padding-top:0; display:unset;">
                        [{*/if*}]
                        [{else}]
                        <button type="button"
                                class="btn btn-default btn-prima btn-basket ladda-button pull-right bntooltip disabled"
                                data-style="expand-right"
                                onclick='alert("[{oxmultilang ident="NOT_BUYABLE"}]");'
                                data-toggle="tooltip" data-placement="top"
                                title="[{oxmultilang ident="NOT_BUYABLE"}]"><span
                                    class="fa fa-shopping-cart"></span>[{*}]<span class="ladda-label">[{oxmultilang ident="TO_THE_BASKET"}]</span>[{*}]
                        </button>
                        [{/if}]
                    </div>
                    <div class="text-right" style="margin-top:10px;">
                        <div>
                            [{oxid_include_dynamic file="widget/product/stockinfo.tpl"}]
                            <strong>
                                <label id="productPrice_[{$iIndex}]" class="price">
                            <span>
                                [{if $_aProduct->isRangePrice()}]
                                    [{oxmultilang ident="PRICE_FROM" }]
                                    [{if !$_aProduct->isParentNotBuyable() }]
                                        [{assign var="oPrice" value=$_aProduct->getMinPrice()}]
                                    [{else}]
                                        [{assign var="oPrice" value=$_aProduct->getVarMinPrice()}]
                                    [{/if}]
                                [{else}]
                                    [{if !$_aProduct->isParentNotBuyable() }]
                                        [{assign var="oPrice" value=$_aProduct->getPrice()}]
                                    [{else}]
                                        [{assign var="oPrice" value=$_aProduct->getVarMinPrice() }]
                                    [{/if}]
                                [{/if}]
                            </span>
                                    [{oxprice price=$oPrice currency=$oView->getActCurrency()}]
                                    [{if $oView->isVatIncluded() }]
                                    [{if !($_aProduct->hasMdVariants() || ($oViewConf->showSelectListsInList() && $_aProduct->getSelections(1)) || $_aProduct->getVariants())}]*[{/if}]
                                    [{/if}]
                                </label>
                        </div>
                    </div>
                </div>


                <div class="col-4">
                    <div class="smallFont">
                        [{oxmultilang ident="ARTNR" suffix="COLON"}] [{$_aProduct.artnum}]
                    </div>
                    [{* Rückrufaktion *}]
                    [{if $_aProduct.bnflagrecall == 1}]<button style="margin-top:5px;height:38px; width:40px;margin-right:5px; margin-left:0 !important;" type="button" class="btn btn-outline-danger bntooltip" data-toggle="tooltip" data-placement="top" title="[{$_aProduct.recallreason}]"><i class="fa fa-exclamation-triangle"></i></button>[{/if}]

                    [{* Aktionsartikel *}]
                    [{if $_aProduct.isAktionbeendet }]<button style="margin-top:5px;height:38px; width:40px;margin-right:5px;margin-left:5px" type="button" class="btn btn-outline-success bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AKTIONBIS"}][{$_aProduct.aktionsende|date_format:"%d.%m.%Y"}]"><i class="fas fa-euro-sign"></i></button>[{/if}]

                    [{* Auslaufartikel *}]
                    [{if $_aProduct.bnflagauslaufartikel == 1}]<button style="margin-top:5px;height:38px; width:40px;margin-right:5px;margin-left:5px" type="button" class="btn btn-auslauf btn-outline-warning bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AUSLAUFARTIKEL"}]"><i class="fa fa-exclamation-triangle"></i></button>[{/if}]

                </div>
                <div class="col-4">
                    <div class="text-right">
                        <input class="form-control amount" type="numeric" name="amountSmall[[{$_aProduct.oxid}]]" onchange="updateAmount(this,'amountSmall[[{$_aProduct.oxid}]]')" value="0" style="text-align: right;width:50px; padding-top:0; display:unset;">
                    </div>
                    <div class="text-right" style="margin-top:10px;">
                        [{oxid_include_dynamic file="widget/product/stockinfo.tpl"}]
                    </div>
                    [{*if $_aProduct.preisneu > 0}]

                    <div class="pull-left" style="margin-top: 2px; margin-left:5px;font-size: small">
								<span style="margin:0">
									<span style="color: red">[{oxprice price=$_aProduct.preisneu currency=$oView->getActCurrency()}]</span>
									<span style="">
										[{oxmultilang ident="PREISNEU"}]
									</span>
								</span>
                    </div>
                    [{/if*}]
                    <label id="productPrice_[{$iIndex}]" class="price pull-right" style="margin:0">
                        [{oxprice price=$_aProduct.price currency=$oView->getActCurrency()}]
                    </label>
                </div>
            </div>
        </div>
        <tr id="article_[{$_aProduct.oxid}]" class="tabellenspalte ">

            <td class="hidden-xs imagecolumn d-none d-xl-table-cell">
                [{*$_aProduct|var_dump*}]
                <div class="pictureBoxVar">
                    <a href="[{*$_productLink*}]#" onclick="loadArticleDetails('[{$_aProduct.oxid}]')" class="viewAllHover glowShadow corners" title="[{$_aProduct.title}]">

                        <img class="lazy-img img-thumbnail" src="https://bodynova.de/out/imagehandler.php?artnum=[{$_aProduct.artnum}]&size=450_450_100" alt="[{$_aProduct.title}]">
                    </a>
                </div>
            </td>
            <td class="ebenetitel d-none d-xl-table-cell">
                <!--<span class="headline"></span>-->
                <div class="pull-left">
                    <div class="amount_input spinner input-group bootstrap-touchspin">
                        <div class="input-group-prepend"><span class="input-group-btn"><button type="button" class="btn btn-default bootstrap-touchspin-down" style="border-top-right-radius:0;border-bottom-right-radius:0">-</button></span></div>
                        <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                        <input class="form-control amount" type="numeric" name="amount[[{$_aProduct.oxid}]]" value="0" style="display: block;">
                        <div style="margin-left:-2px;" class="input-group-append"><span class="input-group-btn"><button type="button" class="btn btn-default bootstrap-touchspin-up" style="border-top-left-radius:0;border-bottom-left-radius:0">+</button></span></div>
                    </div>
                </div>
                <div class="pull-left selection-text ">
                    <strong>[{$_aProduct.artnum}]</strong>
                    &nbsp;[{$_aProduct.title}]

                    [{if $_aProduct.lists}]
                    <table>
                        [{foreach from=$_aProduct.lists item=_list key=_fkey}]
                        <tr>
                            <td>[{$_list.name}]</td>
                            <td>
                                <select name="sel[[{$_aProduct.oxid}]][[{$_fkey}]]">
                                    [{foreach from=$_list item=_item key=_skey}]
                                    [{if $_item->name}]
                                    <option value="[{$_skey}]">[{$_item->name}]</option>
                                    [{/if}]
                                    [{/foreach}]
                                </select>
                            </td>
                        </tr>
                        [{/foreach}]
                    </table>
                    [{/if}]
                </div>
                [{* Verpackungseinheit soll raus }]
					<div class="pull-right" style="margin-top: 2px; margin-left: 5px">
						[{if $_aProduct.veEinheit ne '0' and $_aProduct.veEinheit > 1}]
							<span  style="margin:0">
								[{oxmultilang ident="VERPACKUNGSEINHEIT" suffix="COLON"}] [{$_aProduct.veEinheit}]<a onclick="addVeToInput('[{$_aProduct.oxid}]' , '[{$_aProduct.veEinheit}]')" style="margin-top:0" type="button" class="btn btn-default btn-prima btn-basket ladda-button bntooltip" data-placement="top" title="[{oxmultilang ident="TT-VE-ADD"}]"><span style="width: 5px !important;" class=""><strong>+</strong></span></a>
							</span>
						[{/if}]
						[{*$_aProduct.preisneu}]

					</div>
					[{*}]
            </td>
            [{*}]
					<td class="uvptitle">UVP</td>
					<td class="uvpprice">
						[{oxprice price=$_aProduct.uvpprice currency=$oView->getActCurrency()}]
					</td>
				[{*}]
            <td class="d-none d-xl-table-cell" style="white-space: nowrap">
                [{* Rückrufaktion *}]
                [{if $_aProduct.bnflagrecall == 1}]<button style="" type="button" class="btn btn-xs btn-outline-danger bntooltip" data-toggle="tooltip" data-placement="top" title="[{$_aProduct.recallreason}]"><i class="fa fa-exclamation-triangle"></i></button>[{/if}]

                [{* Aktionsartikel *}]
                [{if $_aProduct.isAktionbeendet }]<button style="margin-left:10px; width:32px" type="button" class="btn btn-xs btn-outline-success bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AKTIONBIS"}][{$_aProduct.aktionsende|date_format:"%d.%m.%Y"}]"><i class="fas fa-euro-sign"></i></button>[{/if}]

                [{* Auslaufartikel *}]
                [{if $_aProduct.bnflagauslaufartikel == 1}]<button style="margin-left:10px; width:32px" type="button" class="btn btn-xs btn-outline-warning bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AUSLAUFARTIKEL"}]"><i class="fa fa-exclamation-triangle"></i></button>[{/if}]

            </td>
            <td class="selprice d-none d-xl-table-cell">
                [{* Anzeige nur bei Admin *}][{* Preis Neu *}]
                [{*if $oxcmp_user->oxuser__oxrights->value eq 'malladmin'*}]
                [{*if $_aProduct.preisneu > 0}]

                <div class="pull-left" style="margin-top: 2px; margin-left:5px;font-size: small">
								<span style="margin:0">
									<span style="color: red">[{oxprice price=$_aProduct.preisneu currency=$oView->getActCurrency()}]</span>
									<span style="">
										[{oxmultilang ident="PREISNEU"}]
									</span>
								</span>
                </div>
                [{/if*}]
                [{*/if*}]
                <label id="productPrice_[{$iIndex}]" class="price" style="margin:0">
                    [{oxprice price=$_aProduct.price currency=$oView->getActCurrency()}]
                </label>
            </td>
            <td class="ampelcolumn d-none d-xl-table-cell">
                [{oxid_include_dynamic file="widget/product/stockinfo.tpl"}]
            </td>
        </tr>
        [{/foreach}]
        <tr class="tabellenspalte" style="margin-top: 20px">
            <td class="Warenkorb" colspan="5">
                <div class="input-line pull-right">
                    <button onclick="addBulkCart('[{$oxid}]')" type="button" style="margin-top: 20px" class="btn btn-default btn-prima btn-basket ladda-button btn-outline-success pull-right"><span class="fa fa-shopping-cart fa-2x"></span></button>
                </div>
            </td>
        </tr>
    </table>
</form>
<script>

    $(function() {
        /*
        $('.lazy-img').each(function(){
            if($(this).visible()){
                $(this).imageloader();
            }
        });
        /*
        $('.lazy-img').imageloader({
            each: function (elm) {
                console.log($(elm).visible());
            },
            callback: function (elm) {
                $(elm).fadeIn();
            }
        });
        */
        //console.log($('.lazy-img').visible());
    });

</script>
