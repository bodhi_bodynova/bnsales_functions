[{capture append="oxidBlock_content"}]
    <div class="card" style="margin-bottom:180px; height:auto">
        <div class="card-header">
            <h1 class="card-title">[{if $oView->order == 'order'}][{oxmultilang ident="PAGE_TITLE_ORDER"}] [{if $oView->orderdate != '01.01.1970'}][{$oView->orderdate}][{/if}][{else}][{oxmultilang ident="BTN_MY_FAVORITES"}][{/if}]</h1>
            <div class="pull-right" style="margin-top: -27px;">
                [{if $oView->getClassName() eq "myfavorites" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="container-fluid">
                    [{include file="widget/product/listNeu.tpl" listId="productList" template='myfavorites'}]
                </div>
            </div>
        </div>
        [{*<div class="card-footer" style="overflow: hidden;">
            <button type="button" class="btn btn-default btn-warenkorb btn-prima btn-basket ladda-button btn-outline-success pull-right bntooltip" data-style="expand-right" onclick="allItemsIntoTheBasket()" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_TO_THE_BASKET"}]"><span style="vertical-align:super;" class="fa fa-shopping-cart"></span>&nbsp;<span style="vertical-align:super;" class="fa fa-shopping-cart"></span><span class="ladda-label">[{oxmultilang ident="TO_THE_BASKET"}]</span></button>

            [{if $isorder}]
            <button type="button" class="btn btn-default btn-warenkorb btn-prima btn-basket ladda-button btn-outline-warning pull-right bntooltip" data-style="expand-right" onclick="resetBasketSelection()" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_RESET_AMOUNT"}]"><span style="vertical-align:super;" class="fa fa-refresh"> [{oxmultilang ident="RESET_AMOUNT"}]</span></button>
            [{/if}]
        </div>*}]
    </div>

    [{/capture}]

[{*capture append="oxidBlock_sidebar"}]
	[{include file="page/account/inc/account_menu.tpl"}]
[{/capture*}]
[{include file="layout/page.tpl" sidebar="Left" sidebarRight=1 sidebarLeft=1}]
