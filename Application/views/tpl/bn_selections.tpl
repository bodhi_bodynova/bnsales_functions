<form>

	[{assign var="preisgruppe" value=$oxcmp_user->getUserPriceData()}]
	[{assign var="preisaktion" value="oxprice"|cat:$preisgruppe|cat:"_aktionspreis"}]
	[{assign var="preisaktionbeginn" value="oxprice"|cat:$preisgruppe|cat:"_aktionsbeginn"}]
	[{assign var="preisaktionende" value="oxprice"|cat:$preisgruppe|cat:"_aktionsende"}]
	[{*$arrVariants|var_dump*}]

	<table class="tableselections table table-hover " border="0" style="margin-bottom: 0">
		<!--<thead>
			<th class="hidden-xs"></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</thead>-->
		[{foreach from=$arrVariants item=_aProduct}]
		[{assign var="preis" value=$_aProduct->getPreisAktuell()}]
		[{assign var="streichpreis" value=$_aProduct->getUserStreichpreis()}]
		[{assign var="preisaktionbeginn" value=$_aProduct->getaktionsbeginn()}]
		[{assign var="preisaktionende" value=$_aProduct->getaktionsende()}]
		<div class="d-xl-none">

			<hr>
			<div class="row">
				<div class="col-4">
					<a href="[{*$_productLink*}]#" onclick="loadArticleDetails('[{$_aProduct->oxarticles__oxid->value}]')" class="viewAllHover glowShadow corners" title="[{$_aProduct->oxarticles__oxtitle->value}]">
						<div class="pictureBoxVar">
							<a href="[{*$_productLink*}]#" onclick="loadArticleDetails('[{$_aProduct->oxarticles__oxid->value}]')" class="viewAllHover glowShadow corners" title="[{$_aProduct->oxarticles__oxtitle->value}]">
								<img class="lazy-img img-thumbnail" src="https://bodynova.de/out/imagehandler.php?artnum=[{$_aProduct->oxarticles__oxartnum->value}]&size=450_450_100" alt="[{$_aProduct->oxarticles__oxtitle->value}]">
							</a>
						</div>
				</div>
				<div class="col-8">
					[{*TODO:VariantenName oder so als Zusatzinfo*}]

					<div class="smallFont">
						<strong>[{oxmultilang ident="ARTNR" suffix="COLON"}]</strong> [{$_aProduct->oxarticles__oxartnum->value}]
					</div>
					<span class="headline">
						[{$_aProduct->oxarticles__oxvarselect->value}]
					</span>
					[{if $_aProduct->oxarticles__verpackungseinheit->value ne '0' and $_aProduct->oxarticles__verpackungseinheit->value > 0}]
					<br>
					<span style="margin:0;cursor:default" class="bntooltip" data-toggle="tooltip" data-original-title=[{oxmultilang ident="VETooltip"}]>
						[{oxmultilang ident="VERPACKUNGSEINHEIT" suffix="COLON"}] [{$_aProduct->oxarticles__verpackungseinheit->value}]
					</span>
					[{/if}]
				</div>

				<div class="col-sm-4 col-5 offset-2 offset-sm-4" style="margin-top:5px;">
					[{* Rückrufaktion *}]
					[{if $_aProduct->oxarticles__bnflagrecall->value == 1}]
					<button style="margin-top:5px;height:38px; width:40px;margin-right:5px; margin-left:0 !important;" type="button" class="btn btn-outline-danger bntooltip" data-toggle="tooltip" data-placement="top" title="[{$_aProduct->oxarticles__recallreason->value}]"><i class="fa fa-exclamation-triangle"></i></button>
					[{/if}]

					[{* Aktionsartikel *}]
					[{*if $_aProduct.isAktionbeendet }]<button style="margin-top:5px;height:38px; width:40px;margin-right:5px;margin-left:5px" type="button" class="btn btn-outline-success bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AKTIONBIS"}][{$_aProduct.aktionsende|date_format:"%d.%m.%Y"}]"><i class="fas fa-euro-sign"></i></button>[{/if*}]

					[{* TODO: Hier schauen... *}]

					[{*$_aProduct->getUserStreichpreis()|var_dump*}]
					[{if $preisaktionbeginn->value <= $smarty.now|date_format:"%Y-%m-%d" && $preisaktionende > $smarty.now|date_format:"%Y-%m-%d" && $streichpreis >0}]
					<button style="margin-top:5px;height:38px; width:40px;margin-right:5px;margin-left:5px" type="button" class="btn btn-outline-success bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AKTIONBIS"}][{$preisaktionende|date_format:"%d.%m.%Y"}]"><i class="fas fa-euro-sign"></i></button>
					[{/if}]


					[{* Auslaufartikel *}]
					[{if $_aProduct->oxarticles__bnflagauslaufartikel->value == 1}]<button style="margin-top:5px;height:38px; width:40px;margin-right:5px;margin-left:5px" type="button" class="btn btn-auslauf btn-outline-warning bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AUSLAUFARTIKEL"}]"><i class="fa fa-exclamation-triangle"></i></button>[{/if}]
				</div>
				<div class="col-sm-4 col-5">
					<div class="text-right">
						[{*$_aProduct|var_dump*}]
						[{*if $_aProduct->isBnBuyable()}]
						[{*if $_aProduct.bnstockflag != 2*}]
						[{if $_aProduct->isBnBuyable()}]
							<input id="inputVar[[{$_aProduct->oxarticles__oxid->value|replace:".":"_"}]" class="form-control amount" type="numeric" name="amountSmall[[{$_aProduct->oxarticles__oxid->value}]]" onchange="updateAmount(this,'amountSmall[[{$_aProduct->oxarticles__oxid->value}]]')" value="0" style="text-align: right;width:50px; padding-top:0; display:unset;">
						[{/if}]

					</div>
					<div class="text-right" style="margin-top:10px;">
						<div>
							[{oxid_include_dynamic file="widget/product/stockinfo.tpl"}]
						</div>

						[{* Preisneu nicht anzeigen... *}]
						[{*if $_aProduct.preisneu > 0}]

							<div class="pull-left" style="margin-top: 2px; margin-left:5px;font-size: small">
									<span style="margin:0">
										<span style="color: red">[{oxprice price=$_aProduct.preisneu currency=$oView->getActCurrency()}]</span>
										<span style="">
											[{oxmultilang ident="PREISNEU"}]
										</span>
									</span>
							</div>
						[{/if*}]
						<label id="productPrice_[{$iIndex}]" class="price pull-right" style="margin:0">
							[{* TODO:: Hier schauen *}]

							[{if $streichpreis > 0 && $streichpreis gt $preis}]
							<s>[{oxprice price=$streichpreis currency=$oView->getActCurrency()}]</s> [{oxprice price=$preis currency=$oView->getActCurrency()}]
							[{else}]
							[{oxprice price=$preis currency=$oView->getActCurrency()}]
							[{/if}]
						</label>
					</div>
				</div>
			</div>
		</div>

		<tr id="article_[{$_aProduct->oxarticles__oxid->value}]" class="tabellenspalte ">

			<td class="hidden-xs imagecolumn d-none d-xl-table-cell">
				[{*$_aProduct|var_dump*}]
				<div class="pictureBoxVar">
					<a href="[{*$_productLink*}]#" onclick="loadArticleDetails('[{$_aProduct->oxarticles__oxid->value}]')" class="viewAllHover glowShadow corners" title="[{$_aProduct->oxarticles__oxtitle->value}]">

						<img class="lazy-img img-thumbnail" src="https://bodynova.de/out/imagehandler.php?artnum=[{$_aProduct->oxarticles__oxartnum->value}]&size=450_450_100" alt="[{$_aProduct->oxarticles__oxtitle->value}]">
					</a>
				</div>
			</td>
			<td class="ebenetitel d-none d-xl-table-cell">
				<!--<span class="headline"></span>-->

				[{* Mengenwähler, falls hier Änderungen vorgenommen werden, bitte in bodynova.js in der Funktion PlusMinusAmount checken, ob die Funktion davon unberührt bleibt *}]
				[{*$_aProduct->isBnBuyable()*}]
				[{if $_aProduct->isBnBuyable()}]
				[{*if $_aProduct.bnstockflag != 2*}]
				<div class="pull-left input-line">
					<div class="amount_input spinner input-group bootstrap-touchspin">
						<span class="input-group-btn"><button onclick="PlusMinusAmount('','0',this);" type="button" class="btn btn-outline-primary bootstrap-touchspin-down" style="border-top-right-radius:0;border-bottom-right-radius:0">-</button></span>
						<span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
						<input id="inputVar[{$_aProduct->oxarticles__oxid->value|replace:".":"_"}]" class="form-control amount" type="numeric" name="amount[[{$_aProduct->oxarticles__oxid->value}]]" value="0" style="text-align:center;display: block;">
						<span class="input-group-btn"><button onclick="PlusMinusAmount('','1',this);" type="button" class="btn btn-outline-primary bootstrap-touchspin-up" style="border-top-left-radius:0;border-bottom-left-radius:0;margin-left:0">+</button></span>
					</div>
				</div>
				[{else}]
				<div class="pull-left" style="width:116px;height:25px"></div>
				[{/if}]


				<div class="pull-left selection-text ">
					<strong>[{$_aProduct->oxarticles__oxartnum->value}]</strong>
					[{*$_aProduct|var_dump*}]
					[{$_aProduct->oxarticles__oxvarselect->value}]




					[{* TODO:: Hier schauen *}]
					[{*if $_aProduct.lists}]
					<table>
						[{foreach from=$_aProduct.lists item=_list key=_fkey}]
						<tr>
							<td>[{$_list.name}]</td>
							<td>
								<select name="sel[[{$_aProduct.oxid}]][[{$_fkey}]]">
									[{foreach from=$_list item=_item key=_skey}]
										[{if $_item->name}]
											<option value="[{$_skey}]">[{$_item->name}]</option>
										[{/if}]
									[{/foreach}]
								</select>
							</td>
						</tr>
						[{/foreach}]
					</table>
					[{/if*}]
				</div>
				[{* Verpackungseinheit soll raus *}]
					<div class="pull-right" style="margin-top: 2px; margin-left: 5px">
						[{if $_aProduct->oxarticles__verpackungseinheit->value ne '0' and $_aProduct->oxarticles__verpackungseinheit->value > 0}]
							<span style="margin:0;cursor:default" class="bntooltip" data-toggle="tooltip" data-original-title=[{oxmultilang ident="VETooltip"}]>
								[{oxmultilang ident="VERPACKUNGSEINHEIT" suffix="COLON"}] [{$_aProduct->oxarticles__verpackungseinheit->value}]
							</span>
						[{/if}]
						[{*$_aProduct.preisneu*}]

					</div>

			</td>
			[{* TODO: Unterteile die divs in einem col, gebe bei Nichtdasein ein Offset mit *}]
			<td class="d-none d-xl-table-cell" style="white-space: nowrap">
				<div class="row">
					[{* Rückrufaktion *}]
					<div class="col-xl-2 offset-xl-2">
						[{if $_aProduct->oxarticles__bnflagrecall->value == 1}]
						<div style="width:32px;" class=" btn btn-xs btn-outline-danger bntooltip" data-toggle="tooltip" data-placement="top" title="[{$_aProduct->oxarticles__recallreason->value}]"><i class="fa fa-exclamation-triangle"></i></div>
						[{/if}]
					</div>

					[{* Aktionsartikel *}]
					<div class="col-xl-2">
						[{*if $_aProduct->isAktionbeendet() }]<button style="margin-left:10px; width:32px" type="button" class="btn btn-xs btn-outline-success bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AKTIONBIS"}][{$_aProduct.aktionsende|date_format:"%d.%m.%Y"}]"><i class="fas fa-euro-sign"></i></button>[{/if*}]
						[{* TODO:: Hier schauen *}]
						[{if $preisaktionbeginn <= $smarty.now|date_format:"%Y-%m-%d" && $preisaktionende > $smarty.now|date_format:"%Y-%m-%d" && $streichpreis >0}]
							<div style="width:32px;" class="btn btn-xs btn-outline-success bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AKTIONBIS"}][{$preisaktionende|date_format:"%d.%m.%Y"}]"><i class="fas fa-euro-sign"></i></div>
						[{/if}]
					</div>
					<div class="col-xl-2">

						[{* Auslaufartikel *}]
						[{if $_aProduct->oxarticles__bnflagauslaufartikel->value == 1}]
						<div style="width:32px;" class="btn btn-xs btn-outline-warning bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AUSLAUFARTIKEL"}]"><i class="fa fa-exclamation-triangle"></i></div>
						[{/if}]
					</div>
				</div>


			</td>
			<td class="selprice d-none d-xl-table-cell">
				[{* Anzeige nur bei Admin *}][{* Preis Neu *}]
				[{*if $oxcmp_user->oxuser__oxrights->value eq 'malladmin'*}]
				[{*if $_aProduct.preisneu > 0}]

				<div class="pull-left" style="margin-top: 2px; margin-left:5px;font-size: small">
					<span style="margin:0">
						<span style="color: red">[{oxprice price=$_aProduct.preisneu currency=$oView->getActCurrency()}]</span>
						<span style="">
							[{oxmultilang ident="PREISNEU"}]
						</span>
					</span>
				</div>
				[{/if*}]
				[{*/if*}]
				<label id="productPrice_[{$iIndex}]" class="price" style="margin:0">
					[{*$_aProduct.$preisaktion}]
					[{$_aProduct.$preisaktionbeginn}]
					[{$_aProduct.$preisaktionende*}]

					[{* TODO::Hier schauen *}]
					[{if $streichpreis > 0 && $streichpreis gt $preis}]
					<s>[{oxprice price=$streichpreis currency=$oView->getActCurrency()}]</s> [{oxprice price=$preis currency=$oView->getActCurrency()}]
					[{else}]
						[{oxprice price=$preis currency=$oView->getActCurrency()}]
					[{/if}]
				</label>
			</td>
			<td class="ampelcolumn d-none d-xl-table-cell">
				[{oxid_include_dynamic file="widget/product/stockinfo.tpl"}]
			</td>
		</tr>
		[{/foreach}]
		<tr class="tabellenspalte" style="margin-top: 20px">
			<td class="Warenkorb" colspan="5">
				<div class="input-line pull-right">
					<button onclick="addBulkCart('[{$oxid}]')" type="button" style="margin-top: 20px" class="btn btn-default btn-prima btn-basket ladda-button btn-outline-success pull-right"><span class="fa fa-shopping-cart fa-2x"></span></button>
				</div>
			</td>
		</tr>
	</table>
</form>
<script>
	$(document).ready(function() {
		if(document.body.offsetWidth<1000){
			$(function() {
				$('.bntooltip').tooltip({
					trigger: "hover",
					delay: {show: 0 , hide: 2500}
				});
			});
		} else {
			$(function() {
				$('.bntooltip').tooltip({
					trigger: "hover",
					delay: {show: 0 , hide: 0}
				});
			});
		}



		/*
		$('.lazy-img').each(function(){
			if($(this).visible()){
				$(this).imageloader();
			}
		});
		/*
		$('.lazy-img').imageloader({
			each: function (elm) {
				console.log($(elm).visible());
			},
			callback: function (elm) {
				$(elm).fadeIn();
			}
		});
		*/
		//console.log($('.lazy-img').visible());
	});
	//});

</script>
