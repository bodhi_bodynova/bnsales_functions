[{capture append="oxidBlock_content"}]
	<div class="panel panel-default">
		<div class="panel-heading">
			<h1 class="panel-title">[{oxmultilang ident="BTN_MY_FAVORITES"}]</h1>
			<div class="pull-right" style="margin-top: -27px;">
				[{if $oView->getClassName() eq "myfavorites" && !$blHideBreadcrumb}]
				[{include file="widget/breadcrumb.tpl"}]
				[{/if}]
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="container-fluid">
					[{include file="widget/product/listNeu.tpl" listId="productList" template='myfavorites'}]
				</div>
			</div>
		</div>
		<div class="panel-footer" style="overflow: hidden;">
			<button type="button"
			        class="btn btn-default btn-warenkorb btn-prima btn-basket ladda-button btn-success pull-right"
			        data-style="expand-right" onclick="allItemsIntoTheBasket()" data-toggle="tooltip"
			        data-placement="bottom" title="[{oxmultilang ident="TIP_TO_THE_BASKET"}]"><span
						class="glyphicon glyphicon-shopping-cart"></span>&nbsp;<span
						class="glyphicon glyphicon-shopping-cart"></span>[{*}]<span class="ladda-label">[{oxmultilang ident="TO_THE_BASKET"}]</span>[{*}]
			</button>

			[{if $isorder}]
			<button type="button"
			        class="btn btn-default btn-warenkorb btn-prima btn-basket ladda-button btn-warning pull-right"
			        data-style="expand-right" onclick="resetBasketSelection()" data-toggle="tooltip"
			        data-placement="bottom" title="[{oxmultilang ident="TIP_RESET_AMOUNT"}]"><span
						class="glyphicon glyphicon-refresh"></span>&nbsp;[{oxmultilang ident="RESET_AMOUNT"}]
			</button>
			[{/if}]
		</div>
	</div>

	[{/capture}]

[{*capture append="oxidBlock_sidebar"}]
	[{include file="page/account/inc/account_menu.tpl"}]
[{/capture*}]
[{include file="layout/page.tpl" sidebar="Left" sidebarRight=1 sidebarLeft=1}]
