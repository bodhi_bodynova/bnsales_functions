<?php

/**
 * Copyright © OXID eSales AG. All rights reserved.
 * See LICENSE file for license details.
 */

$sLangName = "Deutsch";

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'NOT_BUYABLE' => 'temporarily out of stock',
    'EXCEL_PRICEACTION' => 'sale',
    'EXCEL_PRICENEW' => 'price(new)'


];