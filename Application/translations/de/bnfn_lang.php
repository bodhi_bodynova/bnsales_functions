<?php

/**
 * Copyright © OXID eSales AG. All rights reserved.
 * See LICENSE file for license details.
 */

$sLangName = "Deutsch";

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
  'NOT_BUYABLE' => 'Momentan nicht lieferbar.',
    'EXCEL_PRICEACTION' => 'Sale',
    'EXCEL_PRICENEW' => 'Preis(Neu)'
];