<?php

/**
 * Copyright © OXID eSales AG. All rights reserved.
 * See LICENSE file for license details.
 */

$sLangName = "Deutsch";

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'NOT_BUYABLE' => 'Indisponible pour le moment.',
    'EXCEL_PRICEACTION' => 'promo',
    'EXCEL_PRICENEW' => 'nouveau prix'

];