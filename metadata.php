<?php

/**
 * Metadata version
 */
$sMetadataVersion = '2.0';


/**
 * Module information
 */
$aModule = [
    'id'           => 'bnsales_functions',
    'title'        => '<img src="../modules/bodynova/bnsales_functions/out/img/favicon.ico" title="Bodynova Haendlershop Functions">odynova HaendlerShop Funktionen',
    'description'  => [
        'de' => 'Modul zur Bereitstellung der Haendlershop Funktionen',
        'en' => 'Module for sales shop functions'
    ],
    'thumbnail'     => 'out/img/logo_bodynova.png',
    'version'      => '2.0.0',
    'author'       => 'André Bender',
    'email'        => 'a.bender@bodynova.de',
    'controllers'  => [
        /*
        'dre_user_rights'  =>
            \Bender\dre_AdminRights\Application\Controller\Admin\dre_user_rights::class
        */

        'getproductselections'  =>
            \Bodynova\bnSales_Functions\Application\Controller\bn_getProductSelections::class,
        'ajaxbasket'            =>
            \Bodynova\bnSales_Functions\Application\Controller\bn_ajaxbasket::class,
        'myfavorites'           =>
            \Bodynova\bnSales_Functions\Application\Controller\bn_myfavorites::class,
        'pricelist'             =>
            \Bodynova\bnSales_Functions\Application\Controller\bn_pricelist::class


    ],
    'extend'       => [
        /* bb2b:
        'oxarticlelist' => 'bodynova/zzbodynovahaendler/bn_oxarticlelist',
        'account_password'	=> 'bodynova/zzbodynovahaendler/bn_account_password',
        'oxcmp_user'        => 'bodynova/zzbodynovahaendler/bn_oxcmp_user',
        'oxuser'            => 'bodynova/zzbodynovahaendler/bn_oxuser',
        'order'             => 'bodynova/zzbodynovahaendler/bn_order',
        'payment'           => 'bodynova/zzbodynovahaendler/bn_payment',
        'oxarticle'         => 'bodynova/zzbodynovahaendler/bn_oxarticle',
        'oxaddress'         => 'bodynova/zzbodynovahaendler/bn_oxaddress',
        'oxorder'           => 'bodynova/zzbodynovahaendler/bn_oxorder',
        'article_main'      => 'bodynova/zzbodynovahaendler/bn_article_main',
        'oxpicturehandler'  => 'bodynova/zzbodynovahaendler/bn_oxpicturehandler',
        'oxbasket'         	=> 'bodynova/zzbodynovahaendler/bn_oxbasket',
        'oxinputvalidator'  => 'bodynova/zzbodynovahaendler/bn_oxinputvalidator',
        'oxactions'        	=> 'bodynova/zzbodynovahaendler/dre_oxactions',
        'oxprobs_articles'  => 'bodynova/zzbodynovahaendler/bn_oxprobsarticles',???
        'category_main_ajax'=> 'bodynova/zzbodynovahaendler/bn_category_main_ajax',
        'oxnewslist'		=> 'bodynova/zzbodynovahaendler/bn_oxnewslist',
        'oxsession'         => 'bodynova/zzbodynovahaendler/bn_oxsession',
        'oxbasketitem'      => 'bodynova/zzbodynovahaendler/bn_oxbasketitem',
        'order_list'        => 'bodynova/zzbodynovahaendler/bn_order_list',

        'myfavorites'          => 'bodynova/zzbodynovahaendler/bn_myfavorites.php',
        'pricelist'            => 'bodynova/zzbodynovahaendler/bn_pricelist.php',

        */
        /* bn_sales */

        \OxidEsales\Eshop\Application\Model\ArticleList::class                      =>
            \Bodynova\bnSales_Functions\Application\Model\bn_ArticleList::class,

        \OxidEsales\Eshop\Application\Controller\AccountPasswordController::class   =>
            \Bodynova\bnSales_Functions\Application\Controller\bn_AccountPasswordController::class,

        \OxidEsales\Eshop\Application\Component\UserComponent::class                =>
            \Bodynova\bnSales_Functions\Application\Component\bn_UserComponent::class,

        \OxidEsales\Eshop\Application\Model\User::class                             =>
            \Bodynova\bnSales_Functions\Application\Model\bn_User::class,

        \OxidEsales\Eshop\Application\Controller\OrderController::class             =>
            \Bodynova\bnSales_Functions\Application\Controller\bn_OrderController::class,

        \OxidEsales\Eshop\Application\Controller\PaymentController::class                          =>
            \Bodynova\bnSales_Functions\Application\Model\bn_Payment::class,

        \OxidEsales\Eshop\Application\Model\Article::class                          =>
            \Bodynova\bnSales_Functions\Application\Model\bn_Article::class,

        \OxidEsales\Eshop\Application\Model\Address::class                          =>
            \Bodynova\bnSales_Functions\Application\Model\bn_Address::class,

        \OxidEsales\Eshop\Application\Model\Order::class                            =>
            \Bodynova\bnSales_Functions\Application\Model\bn_Order::class,

        \OxidEsales\Eshop\Application\Controller\Admin\ArticleMain::class           =>
            \Bodynova\bnSales_Functions\Application\Controller\Admin\bn_ArticleMain::class,

        \OxidEsales\Eshop\Core\PictureHandler::class                                =>
            \Bodynova\bnSales_Functions\Core\bn_PictureHandler::class,

        \OxidEsales\Eshop\Application\Model\Basket::class                           =>
            \Bodynova\bnSales_Functions\Application\Model\bn_Basket::class,

        \OxidEsales\Eshop\Core\InputValidator::class                                =>
            \Bodynova\bnSales_Functions\Core\bn_InputValidator::class,

        \OxidEsales\Eshop\Application\Model\Actions::class                          =>
            \Bodynova\bnSales_Functions\Application\Model\bn_Actions::class,

        \OxidEsales\Eshop\Application\Controller\Admin\CategoryMainAjax::class      =>
            \Bodynova\bnSales_Functions\Application\Controller\Admin\bn_CategoryMainAjax::class,

        \OxidEsales\Eshop\Application\Model\NewsList::class                         =>
            \Bodynova\bnSales_Functions\Application\Model\bn_NewsList::class,

        \OxidEsales\Eshop\Core\Session::class                                       =>
            \Bodynova\bnSales_Functions\Core\bn_Session::class,

        \OxidEsales\Eshop\Application\Model\BasketItem::class                       =>
            \Bodynova\bnSales_Functions\Application\Model\bn_BasketItem::class,

        \OxidEsales\Eshop\Application\Controller\Admin\OrderOverview::class         =>
            \Bodynova\bnSales_Functions\Application\Controller\Admin\bn_OrderOverview::class,

        \OxidEsales\Eshop\Application\Controller\Admin\OrderList::class             =>
            \Bodynova\bnSales_Functions\Application\Controller\Admin\bn_OrderList::class,


        /* vorlage */
        /*
        \OxidEsales\Eshop\Application\Model\Article::class                      =>
            \Bender\dre_AdminRights\Application\Model\dre_article::class,
        */
    ],
    'templates' => [
        'bn_article_main.tpl'   =>
            'bodynova/bnsales_functions/Application/views/admin/tpl/bn_article_main.tpl',
        'bn_order_overview.tpl' =>
            'bodynova/bnsales_functions/Application/views/admin/tpl/bn_order_overview.tpl',
        'bn_order_list.tpl'     =>
            'bodynova/bnsales_functions/Application/views/admin/tpl/bn_order_list.tpl',
        'bn_selections.tpl'     =>
            'bodynova/bnsales_functions/Application/views/tpl/bn_selections.tpl',
        'bn_bnchild_selections.tpl'     =>
            'bodynova/bnsales_functions/Application/views/tpl/bn_bnchild_selections.tpl',
        'bn_json.tpl'           =>
            'bodynova/bnsales_functions/Application/views/tpl/bn_json.tpl',
        'bn_myfavorites.tpl'    =>
            'bodynova/bnsales_functions/Application/views/tpl/bn_myfavorites.tpl',
        'bn_bnchild_myfavorites.tpl'    =>
            'bodynova/bnsales_functions/Application/views/tpl/bn_bnchild_myfavorites.tpl',
        'bn_acrticledetails.tpl' =>
            'bodynova/bnsales_functions/Application/views/tpl/bn_acrticledetails.tpl',
        /*
        'dre_user_rights.tpl'  =>
            'bender/dre_adminrights/Application/views/admin/tpl/dre_user_rights.tpl',
        */
    ],
    'settings' => [

            /*
            'group' => 'dre_main',
            'name' => 'blDreArShowArticleOwner',
            'type' => 'bool',
            'value' => 'true',
            'position' => 1
            */

    ],
    'events' => [
        /*
        'onActivate' =>
            'Bender\dre_AdminRights\Core\Events::onActivate'
        */
    ],
    'blocks'      => [
    ]
];

?>
